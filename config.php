<?php

/**
 * Database
 */
$config['db'] = Array(
		'host' => 'localhost',
		'username' => 'root',
		'password' => '',
		'db' => 'azstack.co',
		'port' => 3306,
		'charset' => 'utf8',
		'prefix' => 'az_',
);
$config['db_sdk'] = Array(
		'host' => '127.0.0.1',
		'username' => 'root',
		'password' => '',
		'db' => 'azstack_sdk',
		'port' => 3306,
		'charset' => 'utf8',
		'prefix' => 'dh_',
);


$config['baseUrl'] = 'http://localhost/azstack.co/public_html/';
$config['staticUrl'] = 'http://localhost/azstack.co/public_html/static/';

//can config tren static/libs/ckeditor/config.js
//can config tren static/libs/ckfinder/config.php
$config['upload']['path'] = "/azstack.co/public_html/static/uploads/";

$config['languageAll'] = array(
    'en',
    'vi'
);

/**
 * System
 */
$config['template'] = 'default';
$config['mobileTemplate'] = 'default';
$config['language'] = 'en';
$config['enviroment'] = 'development'; // development || production
$config['siteName'] = 'AZStack';
$config['timezone'] = 'America/Lima';
$config['siteOffline'] = false;

$config['memcachedIp'] = '127.0.0.1';
$config['memcachedPort'] = 11211;