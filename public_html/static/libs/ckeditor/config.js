/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function(config) {
   // Define changes to default configuration here. For example:
   // config.language = 'fr';
   // config.uiColor = '#AADC6E';
   config.basicEntities = false;
   config.entities = false;
   config.enterMode = CKEDITOR.ENTER_BR;
   config.shiftEnterMode = CKEDITOR.ENTER_P;
   config.filebrowserBrowseUrl = "/AZStack/azstack.co/public_html/static/libs/ckfinder/ckfinder.html";
   config.filebrowserUploadUrl = "/AZStack/azstack.co/public_html/static/libs/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images";
   
   config.toolbarGroups = [
      {name: 'clipboard', groups: ['clipboard', 'undo']},
      {name: 'links'},
      {name: 'insert'},
      {name: 'tools'},
      {name: 'others'},
      {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
      '/',
      {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align']},
      {name: 'colors'},
      {name: 'styles'},
      {name: 'document', groups: ['mode', 'document', 'doctools']}
   ];
};
