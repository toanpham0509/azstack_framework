<?php

/**
 * Database
 */
$config['db'] = Array(
	'host' => '173.194.243.33',
	'username' => 'azstack',
	'password' => 'duyhaucom22',
	'db' => 'azstack.co',
	'port' => 3306,
        'charset' => 'utf8',
	'prefix' => 'az_',
);
$config['db_sdk'] = Array(
	'host' => '173.194.243.33',
	'username' => 'azstack',
	'password' => 'duyhaucom22',
	'db' => 'azstack_sdk_duong',
	'port' => 3306,
        'charset' => 'utf8',
	'prefix' => 'dh_',
);

$config['baseUrl'] = 'http://test.azstack.com/ftp/toan/azstack.co/public_html/';
$config['staticUrl'] = 'http://test.azstack.com/ftp/toan/azstack.co/public_html/static/';

//can config tren static/libs/ckeditor/config.js
//can config tren static/libs/ckfinder/config.php
$config['upload']['path'] = "/ftp/toan/azstack.co/static/uploads/";
$config['languageAll'] = array(
    'en',
    'vi'
);
/**
 * System
 */
$config['template'] = 'default';
$config['mobileTemplate'] = 'default';
$config['language'] = 'en';
$config['enviroment'] = 'production'; // development || production
$config['siteName'] = 'AZStack';
$config['timezone'] = 'America/Lima';
$config['siteOffline'] = false;

$config['memcachedIp'] = '127.0.0.1';
$config['memcachedPort'] = 11211;