<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminTestimonials">Testimonials</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Testimonials</li>
        </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        $cssClass = "active";
                        foreach($this->languageAll as $language):
                            echo '<li class="'. $cssClass .'"><a href="#lang_testimonials_'. $language .'" data-toggle="tab">'. $language .'</a></li>';
                            $cssClass = "";
                        endforeach;
                    ?>
                </ul>
                <div class="tab-content">
                        <?php
                            $cssClass = "active";
                            foreach($this->languageAll as $language):?>
                                <div class="tab-pane <?= $cssClass ?>" id="lang_testimonials_<?= $language ?>">
                                    <div class="box-body">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                  <th style="width: 10%;">Id</th>
                                                  <th>Content</th>
                                                  <th style="width: 30%;">Write by</th>
                                                  <th style="width: 18%;"><a href="<?= BASE_URL ?>AdminTestimonials/addnew/language/<?= $language ?>">Add new</a></th>
                                                </tr>
                                              </thead>
                                            <tbody>
                                            <?php
                                                foreach($this->testimonials_list[$language] as $item):?>
                                                    <tr>
                                                        <td><?= $item['id'] ?></td>
                                                        <td><?= $item['content'] ?></td>
                                                        <td><?= $item['writeBy'] ?></td>
                                                        <td><a href="<?= BASE_URL ?>AdminTestimonials/edit/id/<?= $item['id'] ?>/language/<?= $language ?>">Edit</a> | <a href="<?= BASE_URL ?>AdminTestimonials/delete/id/<?= $item['id'] ?>/language/<?= $language ?>">Delete</a></td>
                                                    </tr>
                                                    <?php endforeach;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.tab-pane -->

                                <?php $cssClass = "";
                            endforeach;
                        ?>
                    </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->