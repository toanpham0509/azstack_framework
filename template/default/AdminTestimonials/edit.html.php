<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminTestimonials">Testimonials</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Testimonials</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form method="post" role="form">
                <div class="box-body">
                    <div class="form-group">
                        <label>Content</label>
                        <textarea class="ckeditor" name="content" id="content" class="form-control"><?= $this->testimonials['content'] ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Write by</label>
                        <input value="<?= $this->testimonials['writeBy'] ?>" name="writeBy" type="text" class="form-control" placeholder="Write by ..." />
                    </div>

                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->