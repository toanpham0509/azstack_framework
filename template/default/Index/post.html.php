<div id="reassurance_home">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="page-header">
                    <h1 style="font-size: 32px;" class="title"><?= $this->post['title']; ?></h1>
                </div>
                <div class="content">
                    <?= $this->post['content']; ?>
                    <br /><br /><br />
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="page-header">
                    <h1 style="font-size: 32px;" class="title">Relatest Posts</h1>
                </div>
                <ul class="list-group" style="list-style: none; font-size: 16px;">
                    <?php
                        if(isset($this->relatestPosts) && !empty($this->relatestPosts)) {
                            foreach($this->relatestPosts as $relatestPost) {
                                ?>
                                <li>
                                    <a href="<?= BASE_URL ?>Index/post/url/<?= $relatestPost['url'] ?>">
                                        <?= $relatestPost['title'] ?>
                                    </a>
                                </li>
                                <?php
                            }
                        }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>