<div id="slider_main">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <?= $this->lang['index']['header-slide'] ?>
                <?= $this->lang['index']['slide-listoption'] ?><!--#newsList-->
                <div id="quotation" class="ctas box_btn">
                    <a class="btn btn-primary"
                       href="<?= $this->link['index']['get-started'] ?>"
                       data-nivo="lightbox"
                       data-lightbox-type="inline">
                        <?= $this->lang['index']['get-started'] ?>
                    </a>
                    <a class="white-bg cta anim open-modal-alt btn btn-default"
                       id="videosample"
                       href="<?= $this->link['index']['how-it-work'] ?>"
                       data-lightbox-gallery="gallery2">
                        <?= $this->lang['index']['how-it-work'] ?>
                    </a>
                </div>
            </div><!--.col-md-6-->
        </div><!--.row-->
    </div><!--.container-->
</div>
<script type="text/javascript">
    if ($('#slider_main h1 strong').length) {
        $('#slider_main h1 strong').attr("id", "newsData");
    }
    if ($('#slider_main ul').length) {
        $('#slider_main ul').attr("id", "newsList").css("display", "none");
        $('#slider_main ul').newsTicker();
    }
</script>
<!--#slider_main-->
<div id="reassurance_home">
    <div class="container">
        <div class="row" id="gener">
            <?php
                foreach($this->reassurance_types as $type):?>
                    <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading"><?= $this->lang['index'][$type] ?></div>
                        <div class="panel-body">
                            <div class="row box_brand">
                                <?php
                                    foreach($this->reassurance[$type] as $reassurance):
                                        echo '<div class="col-md-4 col-sm-4 col-xs-6"><a href="'. $reassurance['link'] .'"><img src="'. STATIC_URL . $reassurance['icon'] .'" alt="" class="img-responsive"></a></div>';
                                    endforeach;
                                ?>
                            </div><!--.row-->
                        </div><!--.panel-body-->
                    </div><!--.panel-->
                </div><!--.col-md-6-->
                <?php endforeach;
            ?>
        </div><!--.row-->
    </div><!--.container-->
</div><!--#reassurance_home-->
<div id="push">
    <div class="container">
        <div class="row box_push clearfix">
            <?php
                foreach ($this->quality_guarantee_types as $type) :?>
                    <div class="col-md-4 col-sm-4">
                    <h3 class="title"><?= $this->lang['quality_guarantee'][$type] ?></h3>
                    <ul class="list-unstyled">
                        <?php
                            foreach ($this->quality_guarantee[$type] as $value) :
                                echo "<li class='". $value['cssClass'] ."'>". $value['title'] ."</li>";
                            endforeach;
                        ?>
                    </ul>
                </div><!--.col-md-4-->
                <?php endforeach;
            ?>
        </div><!--.row-->
    </div><!--.container-->
</div><!--#push-->
<div id="platform">
    <div class="container">
        <div class="page-header">
            <?= $this->lang['index']['header-platform'] ?>
        </div><!--.page-header-->
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="thumbnail">
                    <img src="<?= STATIC_URL . $this->link['index']['image-download'] ?>" alt="" class="img-responsive">
                    <div class="caption">
                        <p><a href="<?= $this->link['index']['download-learnmore'] ?>" class="btn btn-default"><?= $this->lang['index']['download-learnmore'] ?></a></p>
                    </div><!--.caption-->
                </div><!--.thumbnail-->
            </div><!--.col-md-6-->
            <div class="col-md-6 col-sm-6">
                <p class="text"><?= $this->lang['index']['header-download'] ?></p>
                <p class="app"><a href="<?= $this->link['index']['download-ios'] ?>" class="btn btn-primary"><i class="fa fa-apple"></i><?= $this->lang['index']['download-ios'] ?></a></p>
                <p class="app"><a href="<?= $this->link['index']['download-android'] ?>" class="btn btn-success"><i class="fa fa-android"></i><?= $this->lang['index']['download-android'] ?></a></p>
                <p class="app"><a href="<?= $this->link['index']['view-github'] ?>" class="btn btn-danger"><i class="fa fa-github-alt"></i><?= $this->lang['index']['view-github'] ?></a></p>
            </div><!--.col-md-6-->
        </div><!--.row-->
    </div><!--.container-->
</div><!--#platform-->
<div class="block_link box_11">
    <div class="box_12 box_gener">
        <div class="container">
            <h1 class="title"><?= $this->lang['index']['ready-to-get-started'] ?></h1>
            <ul class="list-unstyled list-inline">
                <li><a href="<?= $this->link['index']['ready-to-get-started-signup'] ?>" class="btn btn-success"><?= $this->lang['index']['ready-to-get-started-signup'] ?></a></li>
                <li><a href="<?= $this->link['index']['ready-to-get-started-talk-to-ceo'] ?>" class="btn btn-success"><?= $this->lang['index']['ready-to-get-started-talk-to-ceo'] ?></a></li>
            </ul>
        </div><!--.container-->
    </div><!--.box_12-->
</div><!--.block_link-->
<div id="service">
    <div class="container top">
        <h3 class="title_01"><?= $this->lang['index']['header-service'] ?></h3>
        <div class="row">
            <?php
                foreach($this->service as $service):?>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="thumbnail">
                        <div class="img_bg"><img src="<?= STATIC_URL . $service['icon'] ?>" alt=""></div>
                        <div class="caption">
                            <h3 class="title_02"><a data-service="service<?= $service['id'] ?>" href="javascript:;"><?= $service['typeName'] ?></a></h3>
                            <p><?= $service['smallName'] ?></p>
                        </div>
                    </div>
                </div><!--.col-md-4-->
                <?php endforeach;
            ?>
        </div><!--.row-->
    </div>
    <!--.container-->
    <?php
        $cssClass = "";
        foreach($this->service as $service):
        ?>
        <div class="service_desc <?= $cssClass ?> container bot" id="service<?= $service['id'] ?>">
            <h3 class="title"><?= $service['title'] ?></h3>
            <?= $service['content'] ?>
            <br />
            <a href="<?= $service['link'] ?>"><?= $service['linkName'] ?></a>
        </div>
        <?php
        $cssClass = "hide";
        endforeach;
    ?>
    <!--.container-->
</div>
<script type="text/javascript">
    $(document).on('click', "a[data-service^='service']", function() {
        $(".service_desc").fadeOut(1000).addClass("hide");
        $("#" + $(this).attr("data-service")).fadeIn(1000).removeClass("hide");
    });
</script>
<!--.service-->
<div class="block_link box_21">
    <div class="box_22 box_gener">
        <div class="container">
            <h1 class="title"><?= $this->lang['index']['find-quality-guarantee'] ?></h1>
            <ul class="list-unstyled list-inline">
                <li><a href="<?= $this->link['index']['find-quality-guarantee-learn-more'] ?>" class="btn btn-success"><?= $this->lang['index']['find-quality-guarantee-learn-more'] ?></a></li>
            </ul>
        </div><!--.container-->
    </div><!--.box_12-->
</div><!--.block_link-->
<div id="pricing">
    <h1 class="title_01"><?= $this->lang['index']['header-pricing'] ?></h1>
    <div class="container">
        <h3 class="title_02"><?= $this->lang['index']['azstack-flatform'] ?></h3>
        <div class="row">
            <?php
                foreach($this->pricing as $pricing):?>
                    <div class="col-md-4 col-sm-4 <?= $pricing['cssClass'] ?>">
                    <div class="thumbnail">
                        <div class="img_h"><img src="<?= STATIC_URL . $pricing['icon'] ?>" alt=""></div>
                        <div class="caption">
                            <h3 class="title"><?= $pricing['typeName'] ?></h3>
                            <p><span class="dolas"><?= $pricing['currency_unit'] ?></span> <span class="price"><?= $pricing['price'] ?></span> <span class="text"><?= $pricing['per_month'] ?></span></p>
                        </div><!--.caption-->
                        <div class="progress content">
                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                <span class="sr-only">60% Complete</span>
                            </div>
                        </div><!--.content-->
                        <div class="quality">
                            <p><?= $pricing['quality'] ?></p>
                            <?= $pricing['content'] ?>
                            <div class="flex align-center">
                                <label class="flex-auto"><?= $pricing['content_bottom'] ?></label>
                                <div class="progress flex-auto"><div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 30%;"></div></div>
                            </div>
                        </div><!--.quality-->
                    </div><!--.thumbnail-->
                </div><!--.col-md-4-->
                <?php endforeach;
            ?>
        </div><!--.row-->
    </div><!--.container-->
</div><!--#pricing-->
<div id="block_order">
    <div class="container">
        <h1 class="title_c"><?= $this->lang['index']['pricing-option-header'] ?></h1>
        <div class="row">
            <?php
                foreach($this->pricingOption as $pricingOption):?>
                    <div class="col-md-3 col-sm-4 col-xs-6">
                        <div class="img_bg"><img src="<?= STATIC_URL . $pricingOption['icon'] ?>" alt=""></div>
                        <h3 class="title"><a href="<?= $pricingOption['link'] ?>"><?= $pricingOption['typeName'] ?></a></h3>
                        <p class="price"><?= $pricingOption['currency_unit'] ?> <span><?= $pricingOption['price'] ?></span> <?= $pricingOption['per_hour'] ?></p>
                    </div><!--.col-md-3-->
                <?php endforeach;
            ?>
        </div><!--.row-->
    </div><!--.container-->
</div><!--#block_order-->
<div class="block_link box_31">
    <div class="box_32 box_gener">
        <div class="container">
            <h1 class="title"><?= $this->lang['index']['header-quotation-again'] ?></h1>
            <ul class="list-unstyled list-inline">
                <li><a href="<?= $this->link['index']['signup-quotation-again'] ?>" class="btn btn-success"><?= $this->lang['index']['signup-quotation-again'] ?></a></li>
                <li><a href="<?= $this->link['index']['meet-ceo-quotation-again'] ?>" class="btn btn-success"><?= $this->lang['index']['meet-ceo-quotation-again'] ?></a></li>
            </ul>
        </div><!--.container-->
    </div><!--.box_12-->
</div><!--.block_link-->
<div id="comment">
    <div class="container">
        <div id="owl-demo-01" class="row clearfix col-xs-12">
            <?php
                foreach($this->testimonials as $testimonials):?>
                    <div class="col-xs-12 item col-md">
                        <?= $testimonials['content'] ?>
                        <a href="javascript:;">- <?= $testimonials['writeBy'] ?></a>
                    </div><!--.col-md-7-->
                <?php endforeach;
            ?>
        </div><!--.row-->
        <script type="text/javascript">
            var owl_01 = $("#owl-demo-01");
            owl_01.owlCarousel({
                items: 1,
                autoPlay: 3000,
                itemsDesktop: [1000, 1], //5 items between 1000px and 901px
                itemsDesktopSmall: [900, 1], // betweem 900px and 601px
                itemsTablet: [600, 1], //2 items between 600 and 0
                itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option
            });
            // Custom Navigation Events
            $(".next-01").click(function() {
                owl_01.trigger('owl.next');
            });
            $(".prev-01").click(function() {
                owl_01.trigger('owl.prev');
            });
        </script>
    </div><!--.container-->
</div><!--#comment-->
<div id="lang_words">
    <div class="container">
        <div class="row">
            <div class="col-md-6" id="map_text">
                <p> <?= $this->countclient ?><?= $this->lang['index']['count-client'] ?></p>
                <p> <?= $this->lang['index']['count-country'] ?></p>
            </div><!--.col-md-6-->
            <div class="col-md-6" id="count">
                <div class="row">
                    <?php 
                        foreach($this->countuser as $countuser):?>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <?php
                                    foreach($countuser as $count):
                                        echo '<span class="one">'. $count .'</span>';
                                    endforeach;
                                ?>
                            </div><!--.col-md-4-->
                        <?php endforeach;
                    ?>
                </div><!--.row-->
                <p><?= $this->lang['index']['header-count-user'] ?></p>
            </div><!--.col-md-6-->
        </div><!--.row-->
    </div><!--.container-->
</div><!--#lang_words-->
<div id="featured">
    <a href="<?= $this->link['index']['featured-in'] ?>"><?= $this->lang['index']['featured-in'] ?></a>
</div><!--#featured-->