<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminService">Service</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Service</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form method="post" role="form">
                <div class="box-body">
                    <input type="hidden" name="icon" value="<?= $this->service['icon'] ?>" />
                    <div class="form-group">
                        <label>Service name</label>
                        <input value="<?= $this->service['typeName'] ?>" name="typeName" type="text" class="form-control" placeholder="Service name ..." />
                    </div>
                    <div class="form-group">
                        <label>Small name</label>
                        <input value="<?= $this->service['smallName'] ?>" name="smallName" type="text" class="form-control" placeholder="Small name ..." />
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <input value="<?= $this->service['title'] ?>" name="title" type="text" class="form-control" placeholder="Title ..." />
                    </div>
                    <div class="form-group">
                        <label>Link name</label>
                        <input value="<?= $this->service['linkName'] ?>" name="linkName" type="text" class="form-control" placeholder="Link name ..." />
                    </div>
                    <div class="form-group">
                        <label>Link</label>
                        <input value="<?= $this->service['link'] ?>" name="link" type="text" class="form-control" placeholder="Link ..." />
                    </div>
                    <div class="form-group">
                        <label>Icon</label>
                        <input
                            value="<?= $this->service['icon'] ?>"
                            name="icon"
                            type="text"
                            class="form-control"
                            placeholder="Icon ..." />
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea class="ckeditor" name="content" id="content" class="form-control"><?= $this->service['content'] ?></textarea>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->