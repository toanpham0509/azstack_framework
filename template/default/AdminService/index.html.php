<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminService">Service</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Service</li>
        </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        $cssClass = "active";
                        foreach($this->languageAll as $language):
                            echo '<li class="'. $cssClass .'"><a href="#lang_service_list_'. $language .'" data-toggle="tab">'. $language .'</a></li>';
                            $cssClass = "";
                        endforeach;
                    ?>
                </ul>
                <form method="post">
                    <div class="tab-content">
                            <?php
                                $cssClass = "active";
                                foreach($this->languageAll as $language):?>
                                    <div class="tab-pane <?= $cssClass ?>" id="lang_service_list_<?= $language ?>">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label>Text Header</label>
                                                <input value="<?= $this->header_service[$language] ?>" name="header-service[<?= $language ?>]" type="text" class="form-control" placeholder="Text Header ..." />
                                            </div>
                                        </div><!-- /.box-body -->
                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                        <div class="box-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                  <tr>
                                                    <th style="width: 5%">Id</th>
                                                    <th style="width: 15%">Name</th>
                                                    <th style="width: 15%">Small Name</th>
                                                    <th style="width: 25%">Title</th>
                                                    <th style="width: 15%">Link Name</th>
                                                    <th style="width: 20%">Link</th>
                                                    <th style="width: 5%">Action</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        foreach($this->service_list[$language] as $item):?>
                                                            <tr>
                                                                <td><?= $item['id'] ?></td>
                                                                <td><?= $item['typeName'] ?></td>
                                                                <td><?= $item['smallName'] ?></td>
                                                                <td><?= $item['title'] ?></td>
                                                                <td><?= $item['linkName'] ?></td>
                                                                <td><a href="<?= $item['link'] ?>"><?= $item['link'] ?></a></td>
                                                                <td><a href="<?= BASE_URL ?>AdminService/edit/id/<?= $item['id'] ?>/language/<?= $language ?>">Edit</a></td>
                                                              </tr>
                                                        <?php endforeach;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div><!-- /.box-body -->
                                    </div><!-- /.tab-pane -->
                                    
                                    <?php $cssClass = "";
                                endforeach;
                            ?>
                    </div><!-- /.tab-content -->
                </form>
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->