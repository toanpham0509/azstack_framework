<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminPricingOption">Pricing option</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Service</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form method="post" role="form">
                <div class="box-body">
                    <input type="hidden" name="icon" value="<?= $this->option['icon'] ?>" />
                    <div class="form-group">
                        <label>Pricing option name</label>
                        <input value="<?= $this->option['typeName'] ?>" name="typeName" type="text" class="form-control" placeholder="Pricing option name ..." />
                    </div>
                    <div class="form-group">
                        <label>Link</label>
                        <input value="<?= $this->option['link'] ?>" name="link" type="text" class="form-control" placeholder="Link ..." />
                    </div>
                    <div class="form-group">
                        <label>Currency unit</label>
                        <input value="<?= $this->option['currency_unit'] ?>" name="currency_unit" type="text" class="form-control" placeholder="Currency unit ..." />
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        <input value="<?= $this->option['price'] ?>" name="price" type="text" class="form-control" placeholder="Price ..." />
                    </div>
                    <div class="form-group">
                        <label>Text per hour</label>
                        <input value="<?= $this->option['per_hour'] ?>" name="per_hour" type="text" class="form-control" placeholder="Text per hour ..." />
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->