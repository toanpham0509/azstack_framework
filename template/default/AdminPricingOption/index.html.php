<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminPricingOption">Pricing Option</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Pricing Option</li>
        </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        $cssClass = "active";
                        foreach($this->languageAll as $language):
                            echo '<li class="'. $cssClass .'"><a href="#lang_option_list_'. $language .'" data-toggle="tab">'. $language .'</a></li>';
                            $cssClass = "";
                        endforeach;
                    ?>
                </ul>
                <form method="post">
                    <div class="tab-content">
                            <?php
                                $cssClass = "active";
                                foreach($this->languageAll as $language):?>
                                    <div class="tab-pane <?= $cssClass ?>" id="lang_option_list_<?= $language ?>">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label>Text Header</label>
                                                <input value="<?= $this->pricing_option_header[$language] ?>" name="pricing-option-header[<?= $language ?>]" type="text" class="form-control" placeholder="Text Header ..." />
                                            </div>
                                        </div><!-- /.box-body -->
                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                        <div class="box-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                  <tr>
                                                    <th style="width: 5%">Id</th>
                                                    <th style="width: 20%">Name</th>
                                                    <th style="width: 15%">Currency Unit</th>
                                                    <th style="width: 15%">Price</th>
                                                    <th style="width: 15%">Text Per hour</th>
                                                    <th style="width: 25%">Link</th>
                                                    <th style="width: 5%">Action</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        foreach($this->option_list[$language] as $item):?>
                                                            <tr>
                                                                <td><?= $item['id'] ?></td>
                                                                <td><?= $item['typeName'] ?></td>
                                                                <td><?= $item['currency_unit'] ?></td>
                                                                <td><?= $item['price'] ?></td>
                                                                <td><?= $item['per_hour'] ?></td>
                                                                <td><?= $item['link'] ?></td>
                                                                <td><a href="<?= BASE_URL ?>AdminPricingOption/edit/id/<?= $item['id'] ?>">Edit</a></td>
                                                              </tr>
                                                        <?php endforeach;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div><!-- /.box-body -->
                                    </div><!-- /.tab-pane -->
                                    
                                    <?php $cssClass = "";
                                endforeach;
                            ?>
                    </div><!-- /.tab-content -->
                </form>
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->