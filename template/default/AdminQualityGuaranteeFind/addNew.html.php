<section class="content-header">
    <h1>
        <?= $this->pageName . " -> Add new language"; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>Admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?= $this->pageName; ?></li>
    </ol>
</section>
<section class="content">
    <br />
    <div class="box box-primary">
        <div class="box-header with-border">
            Add new language
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post" action="">
            <div class="box-body">
                <div class="text-danger"><b><?= $this->message; ?></b></div>
                <div class="form-group">
                    <label for="editor1">Language</label>
                    <input required="required" type="text" name="language" class="form-control" value="<?=
                    $this->language
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="editor1">Title</label>
                        <textarea id="editor1" class="ckeditor" placeholder="Title..." name="title" class="form-control"><?=
                            $this->title;
                            ?></textarea>
                </div>
                <div class="form-group">
                    <label for="signUpText">Learn more Text</label>
                    <input id="signUpText" required="required" type="text" name="signUpText" class="form-control" value="<?=
                    $this->signUpText
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="signUpLink">Learn more Link</label>
                    <input id="signUpLink" required="required" type="text" name="signUpLink" class="form-control" value="<?=
                    $this->signUpLink
                    ?>" />
                </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
                <button  name="submit" type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</section>