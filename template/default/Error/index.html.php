<div class="number">
    <?= $this->code ?>
</div>
<div class="details">
    <h3>Oops! You're lost.</h3>
    <p>
        <?php
        if (!empty($this->debug)) {
            echo "Debug: " . $this->debug;
        }
        if (!empty($this->message)) {
            echo $this->message;
        }
        ?><br/>
        <a href="<?= BASE_URL ?>">
            Return home
        </a>
        or try the search bar below.
    </p>
    <form action="#">
        <div class="input-group input-medium">
            <input type="text" class="form-control" placeholder="keyword...">
            <span class="input-group-btn">
                <button type="submit" class="btn blue"><i class="fa fa-search"></i></button>
            </span>
        </div>
        <!-- /input-group -->
    </form>
</div>