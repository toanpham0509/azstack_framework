<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminFooter">Footer</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Footer</li>
        </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        $cssClass = "active";
                        foreach($this->languageAll as $language):
                            echo '<li class="'. $cssClass .'"><a href="#lang_footer_list_'. $language .'" data-toggle="tab">'. $language .'</a></li>';
                            $cssClass = "";
                        endforeach;
                    ?>
                </ul>
                <form method="post">
                    <div class="tab-content">
                            <?php
                                $cssClass = "active";
                                foreach($this->languageAll as $language):?>
                                    <div class="tab-pane <?= $cssClass ?>" id="lang_footer_list_<?= $language ?>">
                                        <div class="box-body">
                                            <div class="col-md-12">FEATURED IN<br /><br /></div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->featured_in[$language]['value'] ?>" name="featured-in[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->featured_in[$language]['value'] ?> in ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->featured_in[$language]['link'] ?>" name="featured-in[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-12"><br />NAVBAR MENU<br /><br /></div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->blog[$language]['value'] ?>" name="blog[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->blog[$language]['value'] ?> ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->blog[$language]['link'] ?>" name="blog[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->about_us[$language]['value'] ?>" name="about-us[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->about_us[$language]['value'] ?> ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->about_us[$language]['link'] ?>" name="about-us[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->faq[$language]['value'] ?>" name="faq[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->faq[$language]['value'] ?> ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->faq[$language]['link'] ?>" name="faq[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->term_of_use[$language]['value'] ?>" name="term-of-use[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->term_of_use[$language]['value'] ?> ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->term_of_use[$language]['link'] ?>" name="term-of-use[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->privacy_policy[$language]['value'] ?>" name="privacy-policy[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->privacy_policy[$language]['value'] ?> ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->privacy_policy[$language]['link'] ?>" name="privacy-policy[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->dev_are[$language]['value'] ?>" name="dev-are[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->dev_are[$language]['value'] ?> ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->dev_are[$language]['link'] ?>" name="dev-are[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->app_showcase[$language]['value'] ?>" name="app-showcase[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->app_showcase[$language]['value'] ?> ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->app_showcase[$language]['link'] ?>" name="app-showcase[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->contact[$language]['value'] ?>" name="contact[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->contact[$language]['value'] ?> ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->contact[$language]['link'] ?>" name="contact[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->signup[$language]['value'] ?>" name="signup[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->signup[$language]['value'] ?> ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input value="<?= $this->signup[$language]['link'] ?>" name="signup[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-12"><br />ADDRESS<br /><br /></div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input value="<?= $this->office[$language]['value'] ?>" name="office[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->office[$language]['value'] ?> ..." />
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea name="address[<?= $language ?>][value]" class="ckeditor"><?= $this->address[$language]['value'] ?></textarea>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-12"><br />SOCIAL<br /><br /></div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Facebook</label>
                                                    <input value="<?= $this->social_facebook[$language]['link'] ?>" name="social-facebook[<?= $language ?>][link]" type="text" class="form-control" placeholder="Facebook ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Google plus</label>
                                                    <input value="<?= $this->social_google_plus[$language]['link'] ?>" name="social-google-plus[<?= $language ?>][link]" type="text" class="form-control" placeholder="Google plus ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Twitter</label>
                                                    <input value="<?= $this->social_twitter[$language]['link'] ?>" name="social-twitter[<?= $language ?>][link]" type="text" class="form-control" placeholder="Twitter ..." />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Instagram</label>
                                                    <input value="<?= $this->social_instagram[$language]['link'] ?>" name="social-instagram[<?= $language ?>][link]" type="text" class="form-control" placeholder="Instagram ..." />
                                                </div>
                                            </div>
                                        </div><!-- /.box-body -->
                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div><!-- /.tab-pane -->
                                    
                                    <?php $cssClass = "";
                                endforeach;
                            ?>
                    </div><!-- /.tab-content -->
                </form>
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->