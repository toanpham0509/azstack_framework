<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminCountUser">Count User</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Count User</li>
        </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        $cssClass = "active";
                        foreach($this->languageAll as $language):
                            echo '<li class="'. $cssClass .'"><a href="#lang_countuser_list_'. $language .'" data-toggle="tab">'. $language .'</a></li>';
                            $cssClass = "";
                        endforeach;
                    ?>
                </ul>
                <form method="post">
                    <div class="tab-content">
                        <?php
                            $cssClass = "active";
                            foreach($this->languageAll as $language):?>
                                <div class="tab-pane <?= $cssClass ?>" id="lang_countuser_list_<?= $language ?>">
                                    <div class="box-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input value="<?= $this->count_client[$language]['value'] ?>" name="count-client[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->count_client[$language]['value'] ?> ..." />
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input value="<?= $this->count_country[$language]['value'] ?>" name="count-country[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->count_country[$language]['value'] ?> ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input value="<?= $this->header_count_user[$language]['value'] ?>" name="header-count-user[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->header_count_user[$language]['value'] ?> ..." />
                                            </div>
                                        </div>
                                    </div><!-- /.box-body -->
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div><!-- /.tab-pane -->

                                <?php $cssClass = "";
                            endforeach;
                        ?>
                    </div><!-- /.tab-content -->
                </form>
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->