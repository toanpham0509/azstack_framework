<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminQualityGuarantee">Quality Guarantee</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Quality Guarantee</li>
        </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-4">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        $cssClass = "active";
                        foreach($this->languageAll as $language):
                            echo '<li class="'. $cssClass .'"><a href="#lang_high_quality_'. $language .'" data-toggle="tab">'. $language .'</a></li>';
                            $cssClass = "";
                        endforeach;
                    ?>
                </ul>
                <form method="post">
                    <div class="tab-content">
                            <?php
                                $cssClass = "active";
                                foreach($this->languageAll as $language):?>
                                    <div class="tab-pane <?= $cssClass ?>" id="lang_high_quality_<?= $language ?>">
                                        <div class="box-body">
                                          <div class="form-group">
                                            <input name="high-quality[<?= $language ?>]" value="<?= $this->high_quality[$language] ?>" type="text" class="form-control">
                                          </div>
                                        </div><!-- /.box-body -->
                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                        <div class="box-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                  <tr>
                                                    <th style="width: 10%">Id</th>
                                                    <th>Title</th>
                                                    <th style="width: 10%">Action</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        foreach($this->high_quality_list[$language] as $item):?>
                                                            <tr>
                                                                <td><?= $item['id'] ?></td>
                                                                <td><?= $item['title'] ?></td>
                                                                <td><a href="<?= BASE_URL ?>AdminQualityGuarantee/edit/id/<?= $item['id'] ?>">Edit</a></td>
                                                              </tr>
                                                        <?php endforeach;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div><!-- /.box-body -->
                                    </div><!-- /.tab-pane -->
                                    
                                    <?php $cssClass = "";
                                endforeach;
                            ?>
                    </div><!-- /.tab-content -->
                </form>
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
            <div class="col-md-4">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        $cssClass = "active";
                        foreach($this->languageAll as $language):
                            echo '<li class="'. $cssClass .'"><a href="#lang_fast_affordable_'. $language .'" data-toggle="tab">'. $language .'</a></li>';
                            $cssClass = "";
                        endforeach;
                    ?>
                </ul>
                <form method="post">
                    <div class="tab-content">
                            <?php
                                $cssClass = "active";
                                foreach($this->languageAll as $language):?>
                                    <div class="tab-pane <?= $cssClass ?>" id="lang_fast_affordable_<?= $language ?>">
                                        <div class="box-body">
                                          <div class="form-group">
                                            <input name="fast-affordable[<?= $language ?>]" value="<?= $this->fast_affordable[$language] ?>" type="text" class="form-control">
                                          </div>
                                        </div><!-- /.box-body -->
                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                        <div class="box-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                  <tr>
                                                    <th style="width: 10%">Id</th>
                                                    <th>Title</th>
                                                    <th style="width: 10%">Action</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        foreach($this->fast_affordable_list[$language] as $item):?>
                                                            <tr>
                                                                <td><?= $item['id'] ?></td>
                                                                <td><?= $item['title'] ?></td>
                                                                <td><a href="<?= BASE_URL ?>AdminQualityGuarantee/edit/id/<?= $item['id'] ?>">Edit</a></td>
                                                              </tr>
                                                        <?php endforeach;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div><!-- /.box-body -->
                                    </div><!-- /.tab-pane -->
                                    
                                    <?php $cssClass = "";
                                endforeach;
                            ?>
                    </div><!-- /.tab-content -->
                </form>
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
            <div class="col-md-4">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        $cssClass = "active";
                        foreach($this->languageAll as $language):
                            echo '<li class="'. $cssClass .'"><a href="#lang_easily_accessible_'. $language .'" data-toggle="tab">'. $language .'</a></li>';
                            $cssClass = "";
                        endforeach;
                    ?>
                </ul>
                <form method="post">
                    <div class="tab-content">
                            <?php
                                $cssClass = "active";
                                foreach($this->languageAll as $language):?>
                                    <div class="tab-pane <?= $cssClass ?>" id="lang_easily_accessible_<?= $language ?>">
                                        <div class="box-body">
                                          <div class="form-group">
                                            <input name="easily-accessible[<?= $language ?>]" value="<?= $this->easily_accessible[$language] ?>" type="text" class="form-control">
                                          </div>
                                        </div><!-- /.box-body -->
                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                        <div class="box-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                  <tr>
                                                    <th>Id</th>
                                                    <th>Title</th>
                                                    <th style="width: 10%">Action</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        foreach($this->easily_accessible_list[$language] as $item):?>
                                                            <tr>
                                                                <td><?= $item['id'] ?></td>
                                                                <td><?= $item['title'] ?></td>
                                                                <td><a href="<?= BASE_URL ?>AdminQualityGuarantee/edit/id/<?= $item['id'] ?>">Edit</a></td>
                                                              </tr>
                                                        <?php endforeach;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div><!-- /.box-body -->
                                    </div><!-- /.tab-pane -->
                                    
                                    <?php $cssClass = "";
                                endforeach;
                            ?>
                    </div><!-- /.tab-content -->
                </form>
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->