<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminQualityGuarantee">Quality Guarantee</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Quality Guarantee</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form method="post" role="form">
                <div class="box-body">
                    
                    <input type="hidden" name="type" value="<?= $this->qualityguarantee['type'] ?>" />
                    <input type="hidden" name="cssClass" value="<?= $this->qualityguarantee['cssClass'] ?>" />
                    <div class="form-group">
                        <label>Title</label>
                        <input value="<?= $this->qualityguarantee['title'] ?>" name="title" type="text" class="form-control" placeholder="Title ..." />
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->