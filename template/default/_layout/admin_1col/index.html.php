<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include TPL_PATH . '_layout/admin_header_login.html.php' ?>
	</head>
	<body class="<?= $this->layout->bodyClass; ?>">
		<?= $this->layout->content ?>
		<?php include TPL_PATH . '_layout/admin_footer_login.html.php' ?>
	</body>
</html>