<?php
/**
 * Class Layout
 */
class Layout {

	/**
	 * @param Layout Object $layout
	 * @param String $layoutPath
	 */
	public static function run($layout, $layoutPath, View $view, Controller $controller) {
		self::projectRender($view, $layout, $controller);
	}

	/**
	 * @param View $view
	 * @param $layout
	 * @param $controller
	 */
	private static function projectRender(View $view, $layout, $controller){
		//check signed in
		if ($_SESSION['azs']
				&& isset($_SESSION['azs']['sessionCode'])
				&& isset($_SESSION['azs']['user'])
				&& !empty($_SESSION['azs']['user']) ){
			$controller->layout->userName = $_SESSION['azs']['user']['name'];
			$controller->layout->email = $_SESSION['azs']['user']['email'];
		} else {
			if(isset($controller->layout->pageId)
					&& $controller->layout->pageId == "AdminLogin") {
			} else {
				header("Location: " . BASE_URL . "AdminLogin");
				exit();
			}
		}
	}
}