<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
    <a class="navbar-brand" href="<?= BASE_URL ?>"><img src="<?= STATIC_URL ?>images/logo.png" alt=""></a>
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav navbar-right" id="menu">
        <li>
            <a href="javascript:;"><?= $this->layout->lang['navbar']['language'] ?></a>
        </li>
        <li><a href="<?= $this->layout->link['navbar']['features'] ?>"><span><?= $this->layout->lang['navbar']['features'] ?></span></a></li>
        <li><a href="<?= $this->layout->link['navbar']['free-download'] ?>"><span><?= $this->layout->lang['navbar']['free-download'] ?></span></a></li>
        <li><a href="<?= $this->layout->link['navbar']['about-us'] ?>"><span><?= $this->layout->lang['navbar']['about-us'] ?></span></a></li>
        <li><a href="<?= $this->layout->link['navbar']['pricing'] ?>"><span><?= $this->layout->lang['navbar']['pricing'] ?></span></a></li>
        <li class="active"><a target='_blank' href="<?= $this->layout->link['navbar']['login'] ?>"><span><?= $this->layout->lang['navbar']['login'] ?></span></a></li>
    </ul>
</div>
<!--.navbar-collapse-->