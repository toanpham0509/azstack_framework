<footer id="footer">
    <div class="container">
        <div class="top navbar">
            <div class="container-fluid">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="<?= $this->layout->link['footer']['blog'] ?>"><?= $this->layout->lang['footer']['blog'] ?></a></li>
                    <li><a href="<?= $this->layout->link['footer']['about-us'] ?>"><?= $this->layout->lang['footer']['about-us'] ?></a></li>
                    <li><a href="<?= $this->layout->link['footer']['faq'] ?>"><?= $this->layout->lang['footer']['faq'] ?></a></li>
                    <li><a href="<?= $this->layout->link['footer']['term-of-use'] ?>"><?= $this->layout->lang['footer']['term-of-use'] ?></a></li>
                    <li><a href="<?= $this->layout->link['footer']['privacy-policy'] ?>"><?= $this->layout->lang['footer']['privacy-policy'] ?></a></li>
                    <li><a href="<?= $this->layout->link['footer']['dev-are'] ?>"><?= $this->layout->lang['footer']['dev-are'] ?></a></li>
                    <li><a href="<?= $this->layout->link['footer']['app-showcase'] ?>"><?= $this->layout->lang['footer']['app-showcase'] ?></a></li>
                    <li><a href="<?= $this->layout->link['footer']['contact'] ?>"><?= $this->layout->lang['footer']['contact'] ?></a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?= $this->layout->link['footer']['signup'] ?>" class="btn btn-primary"><?= $this->layout->lang['footer']['signup'] ?></a></li>
                </ul>
            </div><!--.container-fluid-->
        </div><!--.top-->
        <div class="bot">
            <address>
                <h1 class="title_01">AZStack Pte. Ltd.</h1>
                <p class="title_02"><?= $this->layout->lang['footer']['office'] ?>:</p>
                <?= $this->layout->lang['footer']['address'] ?>
            </address>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <ul class="list-inline">
                        <li><a href="<?= $this->layout->link['footer']['social-facebook'] ?>"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="<?= $this->layout->link['footer']['social-google-plus'] ?>"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="<?= $this->layout->link['footer']['social-twitter'] ?>"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="<?= $this->layout->link['footer']['social-instagram'] ?>"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div><!--.col-md-6-->
                <div class="col-md-6 col-sm-6">Copyright 2016 © <b>AZStack Pte. Ltd</b>. - All rights reserved</div><!--.col-md-6-->
            </div><!--.row-->
        </div><!--.bot-->
    </div><!--.container-->
</footer><!--#footer-->