<header class="main-header">
	<!-- Logo -->
	<a href="<?= BASE_URL ?>Admin" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>Az</b></span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>AzStack</b> Admin</span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?= STATIC_URL ?>admin/dist/img/user2-160x160.jpg" class="user-image" alt="User Image" />
						<span class="hidden-xs"><?= $this->layout->userName; ?></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header">
							<img src="<?= STATIC_URL ?>admin/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
							<p>
								<?= $this->layout->userName ?> - <?= $this->layout->type ?>
								<small>Login time: <?= date("H:i:s d/M/Y", $this->layout->loginTime) ?></small>
							</p>
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="#" class="btn btn-default btn-flat">Profile</a>
							</div>
							<div class="pull-right">
								<a href="<?= BASE_URL ?>AdminLogout" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<ul class="sidebar-menu">
			<li class="<?= $this->layout->menuAdminIndexClass ?>">
				<a href="<?= BASE_URL ?>Admin">
					<i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
				</a>
			</li>
			<li class="treeview <?=
				$this->layout->menuLayoutPlatformClass
				. $this->layout->menuLayoutSliderClass
				. $this->layout->menuLayoutGetStartedClass
				. $this->layout->menuAdminQualityGuaranteeFindClass
				. $this->layout->menuAdminLayOutFreeProgramClass
				. $this->layout->menuLayoutNavClass
				. $this->layout->menuAdminReassuranceClass
				. $this->layout->menuAdminQualityGuaranteeClass
				. $this->layout->menuAdminServiceClass
				. $this->layout->menuAdminPricingClass
				. $this->layout->menuAdminPricingOptionClass
				. $this->layout->menuAdminTestimonialsClass
				. $this->layout->menuAdminCountUserClass
				. $this->layout->menuAdminFooterClass;
			?>">
				<a href="#">
					<i class="fa fa-files-o"></i>
					<span>Layout Options</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li class="<?= $this->layout->menuLayoutNavClass; ?>">
						<a href="<?= BASE_URL ?>AdminNavbar">
							<i class="fa fa-circle-o"></i>
							Admin Navbar
						</a>
					</li>
					<li class="<?= $this->layout->menuAdminReassuranceClass; ?>">
						<a href="<?= BASE_URL ?>AdminReassurance">
							<i class="fa fa-circle-o"></i>
							Admin Reassurance
						</a>
					</li>
					<li class="<?= $this->layout->menuLayoutSliderClass; ?>">
						<a href="<?= BASE_URL ?>AdminLayoutSlider">
							<i class="fa fa-circle-o"></i>
							Slider
						</a>
					</li>
					<li class="<?= $this->layout->menuAdminQualityGuaranteeClass; ?>">
						<a href="<?= BASE_URL ?>AdminQualityGuarantee"><i class="fa fa-circle-o"></i>
							Admin Quality Guarantee
						</a>
					</li>
					<li class="<?= $this->layout->menuLayoutPlatformClass; ?>">
						<a href="<?= BASE_URL ?>AdminLayoutPlatform"><i class="fa fa-circle-o"></i>
							Platform
						</a>
					</li>
					<li class="<?= $this->layout->menuLayoutGetStartedClass ?>">
						<a href="<?= BASE_URL ?>AdminLayoutGetStarted"><i class="fa fa-circle-o"></i>
							Get Started
						</a>
					</li>
					<li class="<?= $this->layout->menuAdminServiceClass ?>">
						<a href="<?= BASE_URL ?>AdminService"><i class="fa fa-circle-o"></i>
							Services
						</a>
					</li>
					<li class="<?= $this->layout->menuAdminQualityGuaranteeFindClass ?>">
						<a href="<?= BASE_URL ?>AdminQualityGuaranteeFind">
							<i class="fa fa-circle-o"></i>
							Quality guarantee find
						</a>
					</li>
					<li class="<?= $this->layout->menuAdminPricingClass ?>">
						<a href="<?= BASE_URL ?>AdminPricing">
							<i class="fa fa-circle-o"></i>
							Our Pricing
						</a>
					</li>
					<li class="<?= $this->layout->menuAdminPricingOptionClass ?>">
						<a href="<?= BASE_URL ?>AdminPricingOption">
							<i class="fa fa-circle-o"></i>
							Pricing Option
						</a>
					</li>
					<li class="<?= $this->layout->menuAdminLayOutFreeProgramClass ?>">
						<a href="<?= BASE_URL ?>AdminLayOutFreeProgram">
							<i class="fa fa-circle-o"></i>
							Free Program
						</a>
					</li>
					<li class="<?= $this->layout->menuAdminTestimonialsClass ?>">
						<a href="<?= BASE_URL ?>AdminTestimonials">
							<i class="fa fa-circle-o"></i>
							Timonials
						</a>
					</li>
					<li class="<?= $this->layout->menuAdminCountUserClass ?>">
						<a href="<?= BASE_URL ?>AdminCountUser">
							<i class="fa fa-circle-o"></i>
							Count User, Country
						</a>
					</li>
					<li class="<?= $this->layout->menuAdminFooterClass ?>">
						<a href="<?= BASE_URL ?>AdminFooter">
							<i class="fa fa-circle-o"></i>
							Foooter
						</a>
					</li>

				</ul>
			</li>
			<li class="treeview <?=
				$this->layout->menuAdminPostCategoryAddNewClass
				. $this->layout->menuAdminPostCategoryClass
				. $this->layout->menuAdminPostClass
				. $this->layout->menuAdminPostAddNewClass
			?>">
				<a href="#">
					<i class="fa fa-pie-chart"></i>
					<span>Posts</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>
				<ul class="treeview-menu">
					<li class="<?= $this->layout->menuAdminPostClass ?>">
						<a href="<?= BASE_URL ?>/AdminPost">
							<i class="fa fa-circle-o"></i> All Posts
						</a>
					</li>
					<li class="<?= $this->layout->menuAdminPostAddNewClass ?>">
						<a href="<?= BASE_URL ?>AdminPost/addNew">
							<i class="fa fa-circle-o"></i> Add new post
						</a>
					</li>
					<li class="<?= $this->layout->menuAdminPostCategoryClass ?>">
						<a href="<?= BASE_URL ?>AdminPostCategory">
							<i class="fa fa-circle-o"></i> Categories
						</a>
					</li>
				</ul>
			</li>
			<li class="">
				<a href="<?= BASE_URL ?>" target="_blank">
					<i class="fa fa-dashboard"></i> <span>View front page</span></i>
				</a>
			</li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>