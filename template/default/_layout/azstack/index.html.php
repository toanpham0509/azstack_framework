<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include TPL_PATH . '_layout/header.html.php';  ?>
    </head>
    <body data-spy="scroll" data-target="#menu_nav" data-offset="0">
        <nav class="navbar navbar-default" id="menu_nav" data-spy="affix" data-offset-top="73">
            <div class="container">
                <?php include TPL_PATH . '_layout/navbar.html.php';  ?>
            </div><!--.container-->
        </nav><!--#menu_nav-->
        <?= $this->layout->content ?>
        <?php include TPL_PATH . '_layout/footer.html.php';  ?>
    </body>
</html>