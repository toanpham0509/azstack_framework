<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include TPL_PATH . '_layout/admin_header.html.php' ?>
	</head>
	<body class="skin-blue sidebar-mini <?= $this->layout->bodyClass; ?>">
		<div class="wrapper">
			<?php include TPL_PATH . '_layout/admin_navbar.html.php' ?>
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<div class="right_main_content">
					<?= $this->layout->content ?>
				</div>
			</div>
			<?php include TPL_PATH . '_layout/admin_footer.html.php' ?>
			<div class="control-sidebar-bg"></div>
		</div><!-- ./wrapper -->
	</body>
</html>