<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?include TPL_PATH.'_layout/admin_header.html.php'?>
    </head>
    <body>
		<div class="navbar">
			<div class="container">
				<div class="logo"><a href="<?=BASE_URL?>"><img src="<?=STATIC_URL?>image/onChat_icon.png"></a></div>
				<ul class="topnav">
					<li>
						<?if($_SESSION['user']){?>
							<b><a href="<?=BASE_URL?>mymusic/rbtmanager"><?=$_SESSION['user']->fullname?></a></b>
						<?}else{?>
							<a href="<?=BASE_URL?>user/login">Đăng nhập</a>
						<?}?>
					</li>
					
					<li>|</li>
					
					<li><a href="<?=BASE_URL?>info">Liên hệ</a></li>
					
					<li>|</li>
					
					<li><a href="<?=BASE_URL?>download">Tải về</a></li>
					
					<li>|</li>
					
					<li><a href="<?=BASE_URL?>">Trang chủ</a></li>
					
					
				</ul>
			</div>
		</div>
		
		<div class="main">
			<div class="container">
				<?=$this->layout->content?>
			</div>
			<br class="clear">
		</div>

        <?include TPL_PATH.'_layout/admin_footer.html.php'?>
    </body>
</html>

