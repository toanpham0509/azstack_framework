<meta charset="UTF-8">
<title>AZStack - <?=$this->layout->title?></title>
<meta content='mobile messaging platform, add chat feature to any app ' lang='en' name='description'>
<meta content='azstack,messaging,platform,communications,internet,open,chat,messages,mobile,app,developers,SDK,API,iphone,iOS,Android,real-time,async' lang='en' name='keywords'>
<meta content='AZStack.co' property='og:site_name'>
<meta content='website' property='og:type'>
<meta content='AZStack - Communication platform for mobile applications' property='og:title'>
<meta content='mobile messaging platform, add chat feature to any app' property='og:description'>
<meta content='https://azstack.co/' property='og:url'>

<link href="<?= STATIC_URL ?>images/favicon.ico" rel="shortcut icon">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--CSS-->
<link href="<?= STATIC_URL ?>libs/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?= STATIC_URL ?>libs/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?= STATIC_URL ?>plugins/owl-carousel/style.css" rel="stylesheet" type="text/css">
<link href="<?= STATIC_URL ?>css/style.css" rel="stylesheet" type="text/css">
<link href="<?= STATIC_URL ?>css/nivo-lightbox/nivo-lightbox.css" media="screen" rel="stylesheet" type="text/css" />
<link href="<?= STATIC_URL ?>css/nivo-lightbox/themes/default/default.css" media="screen" rel="stylesheet" type="text/css" />
<!--JS-->
<script src="<?= STATIC_URL ?>libs/jquery/1.11.2/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="<?= STATIC_URL ?>libs/bootstrap-3.3.6-dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= STATIC_URL ?>plugins/newsticker/newsTicker.js" type="text/javascript"></script>
<script src="<?= STATIC_URL ?>plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?= STATIC_URL ?>js/function.js" type="text/javascript"></script>
<script src="<?= STATIC_URL ?>js/nivo-lightbox.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#videosample').nivoLightbox({ effect: 'fade' });
    });
</script>