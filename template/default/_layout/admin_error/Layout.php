<?php

/**
 * @author Dau Ngoc Huy - huydaungoc@gmail.com - anhxtanh3087
 */
class Layout {

	public static function run($layout, $layoutPath, View $view, Controller $controller) {
		$layout->footer = $view->fetch($layoutPath . '../admin_footer.html.php');
	}

}
