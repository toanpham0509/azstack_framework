<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><?= $this->layout->title ?> - AZStack</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="<?= STATIC_URL ?>libs/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="#"/>
    <style>
        .page-404-full-page {
            overflow-x: hidden;
            background-color: #fafafa !important;
        }
        body {
            color: #000;
            font-family: 'Open Sans', sans-serif;
            padding: 0px !important;
            margin: 0px !important;
            font-size: 13px;
            direction: ltr;
        }
        ::-webkit-scrollbar {
            width: 12px;
        }
        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            box-shadow: none;
            border: 0;
        }
        ::-webkit-scrollbar-thumb {
            background-color: #cecece;
        }
        ::-webkit-scrollbar-track {
            border-radius: 0;
            box-shadow: none;
            border: 0;
        }
        ::-webkit-scrollbar-track {
            background-color: #eaeaea;
        }
        div, input, select, textarea, span, img, table, td, th, p, a, button, ul, code, pre, li {
            -webkit-border-radius: 0 !important;
            -moz-border-radius: 0 !important;
            border-radius: 0 !important;
        }
        .page-404-full-page .page-404 {
            margin-top: 100px;
        }
        .page-404 {
            text-align: center;
        }
        .page-404 .number {
            position: relative;
            top: 35px;
            display: inline-block;
            letter-spacing: -10px;
            margin-top: 0px;
            margin-bottom: 10px;
            line-height: 128px;
            font-size: 128px;
            font-weight: 300;
            color: #7bbbd6;
            text-align: right;
        }
        .page-404 .details {
            margin-left: 40px;
            display: inline-block;
            padding-top: 0px;
            text-align: left;
        }
        h1, h2, h3, h4, h5, h6 {
            font-family: 'Open Sans', sans-serif;
            font-weight: 300 !important;
        }
        h3, .h3 {
            font-size: 24px;
        }
        a, a:focus, a:hover, a:active {
            outline: 0;
        }
        a {
            text-shadow: none !important;
            color: #0d638f;
        }
        .input-medium {
            width: 240px !important;
        }
        .page-404-full-page .details input {
            background-color: #ffffff;
        }
        .form-control {
            font-size: 14px;
            font-weight: normal;
            color: #333333;
            background-color: #ffffff;
            border: 1px solid #e5e5e5;
            border-radius: 0;
            -webkit-box-shadow: none;
            box-shadow: none;
            -webkit-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }
        .btn > i {
            font-size: 14px;
        }
        [class^="fa-"], [class*=" fa-"] {
            display: inline-block;
            margin-top: 1px;
            font-size: 14px;
            line-height: 14px;
        }
        .fa {
            display: inline-block;
            font-family: FontAwesome;
            font-style: normal;
            font-weight: normal;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        .fa-search:before {
            content: "Search";
        }
        .btn.blue {
            color: white;
            text-shadow: none;
            background-color: #4d90fe;
        }
    </style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-404-full-page">
<div class="row">
    <div class="col-md-12 page-404">
        <?= $this->layout->content ?>
    </div>
</div>
</body>
<!-- END BODY -->
</html>