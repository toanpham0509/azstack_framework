<?php

/**
 * @author Dau Ngoc Huy - huydaungoc@gmail.com - anhxtanh3087
 */
class Layout {

	/**
	 * @param Layout Object $layout
	 * @param String $layoutPath
	 */
	public static function run($layout, $layoutPath, View $view, Controller $controller) {
		self::projectRender($view, $layout, $controller);
	}

	/**
	 * @param View $view
	 * @param $layout
	 * @param $controller
	 */
	private static function projectRender(View $view, $layout, $controller){
	}

}
