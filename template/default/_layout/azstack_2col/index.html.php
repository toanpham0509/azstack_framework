<!DOCTYPE html>
<html>
    <head>
        <title><?= $this->layout->title ?> - AZStack</title>
        <meta charset="utf-8">
        <meta name="description" content="AZStack">
        <meta name="keywords" content="AZStack">
        <meta name="author" content="ASZtack.co">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--if lt IE 9script(src='http://html5shim.googlecode.com/svn/trunk/html5.js')-->
        <link rel="shortcut icon" href="#">
        <link rel="apple-touch-icon" href="#">
        <link rel="apple-touch-icon" sizes="72x72" href="#">
        <link rel="apple-touch-icon" sizes="114x114" href="#">
        <script src="<?= STATIC_URL ?>libs/jquery/1.11.3/jquery-1.11.3.min.js"></script>
        <link rel="stylesheet" href="<?= STATIC_URL ?>libs/bootstrap-3.3.6-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= STATIC_URL ?>css/project.css">
        <script src="<?= STATIC_URL ?>libs/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
        <script src="<?= STATIC_URL ?>libs/ckeditor/ckeditor.js"></script>
        <script src="<?= STATIC_URL ?>libs/ckfinder/ckfinder.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <div class="modal-wrapper"></div>
            <header>
                <div class="wrapper">
                    <div class="home"><a href="<?= BASE_URL ?>" data-mixpanel="home" data-bypass class="mixpanel-event"></a></div>
                    
                    <div class="nav">
                        <ul>
                            <li><a data-dropdown="account-dropdown" href="javascript:;" class="account">Trần Hồng Dương<span class="arrowcustom"></span></a></li>
                        </ul>
                    </div>
                    <div class="dropdown">
                        <div id="account-dropdown">
                            <ul>
                                <li class="first"><a href="http://localhost/AZStack/AZStackBackend/public_html/user">Account info</a></li>
                                <li><a href="http://localhost/AZStack/AZStackBackend/public_html/user/changepassword">Change password</a></li>
                                <li><a href="http://localhost/AZStack/AZStackBackend/public_html/user/logout" class="js-logout">Log out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
            <script>
                $(document).ready(function() {
                    $(".arrowcustom, .arrow").parent().click(function() {
                        var dropdown = $("#" + $(this).attr("data-dropdown"));
                        if (dropdown.hasClass("show")) {
                            dropdown.removeClass("show");
                            dropdown.css("visibility", "hidden");
                        } else {
                            dropdown.css("display", "block");
                            dropdown.addClass("show");
                            dropdown.css("visibility", "visible");
                            $("div[class=dropdown]>div").each(function() {
                                if (dropdown.attr("id") !== $(this).attr("id")) {
                                    $(this).css("display", "none");
                                }
                            });
                        }
                    });
                    $(".arrowcustom, .arrow").parent().blur(function() {
                        var dropdown = $("#" + $(this).attr("data-dropdown"));
                        dropdown.removeClass("show");
                    });
                });
            </script>            <div id="content">
                <div id="content-spacer-top"></div>
                <div id="content-inner show">
                    <div class="notification"></div>
                    <main>
                        <div class="main-sidebar">
                            <div>
                                <ul class="top-space">
                                    <li class="custom adminreport ">
                                        <a href="http://localhost/AZStack/AZStackBackend/public_html/report">Admin Analytics</a>
                                    </li>
                                    <li class="custom article active">
                                        <a href="http://localhost/AZStack/AZStackBackend/public_html/article">Article</a>
                                    </li>
                                </ul>
                                <ul>
                                    <li class="analytics ">
                                        <a href="http://localhost/AZStack/AZStackBackend/public_html/project/analytics/id/">Analytics</a>
                                    </li>
                                    <li class="keys ">
                                        <a href="http://localhost/AZStack/AZStackBackend/public_html/project/keys/id/">Keys</a>
                                    </li>
                                    <li class="custom authenurl ">
                                        <a href="http://localhost/AZStack/AZStackBackend/public_html/project/authenurl/id/">Authen URL</a>
                                    </li>
                                </ul>
                                <ul>
                                    <li class="push ">
                                        <a href="http://localhost/AZStack/AZStackBackend/public_html/project/push/id/">Push</a>
                                    </li>
                                    <li class="tools ">
                                        <a href="http://localhost/AZStack/AZStackBackend/public_html/project/tools/id/">Tools</a>
                                    </li>
                                </ul>
                                <ul>
                                    <li class="info ">
                                        <a href="http://localhost/AZStack/AZStackBackend/public_html/project/info/id/">Project info</a>
                                    </li>
                                    <li class="logs ">
                                        <a href="http://localhost/AZStack/AZStackBackend/public_html/project/delete/id/">Delete project</a>
                                    </li>
                                </ul>                            </div>
                        </div>
                        <div class="main-content">
                            <style>
                                .table {
                                    font-size: 14px;
                                }
                                .table th, td {
                                    padding: 5px!important;
                                }
                                .asc {
                                    width: 15px;
                                    height: 10px;
                                    display: inline-block;
                                    background: url('http://localhost/AZStack/AZStackBackend/static/img/asc.gif') no-repeat right;
                                }
                                .desc {
                                    width: 15px;
                                    height: 10px;
                                    display: inline-block;
                                    background: url('http://localhost/AZStack/AZStackBackend/static/img/desc.gif') no-repeat right;
                                }
                                .table {
                                    width: 839px!important;
                                }
                                .table img{
                                    width: 50px;
                                }
                                .middle {
                                    vertical-align: middle!important;
                                }
                                .c-gray-box {
                                    width: 839px!important;
                                }
                                .modal-wrapper .hide{
                                    display: block!important;
                                }
                                .editarticle{
                                    background-position: 4px -182px;
                                    background-image: url("http://localhost/AZStack/AZStackBackend/static/img/sidebar_custom.png");
                                    background-repeat: no-repeat;
                                    padding-left: 25px;
                                }
                                .deletearticle{
                                    background-position: 4px -215px;
                                    background-image: url("http://localhost/AZStack/AZStackBackend/static/img/sidebar_custom.png");
                                    background-repeat: no-repeat;
                                    padding-left: 25px;
                                }
                            </style>
                            <div class="analytics-usage">
                                <br />
                                <table class="table table-hover table-condensed table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th width="250px"><a href="http://localhost/AZStack/AZStackBackend/public_html/article?page=1&sortColumn=title&sortType=asc">Title</a><span class=""></span></th>
                                            <th><a href="http://localhost/AZStack/AZStackBackend/public_html/article?page=1&sortColumn=description&sortType=asc">Description</a><span class=""></span></th>
                                            <th><a href="http://localhost/AZStack/AZStackBackend/public_html/article?page=1&sortColumn=image&sortType=asc">Image</a><span class=""></span></th>
                                            <th width="105px" style="text-align: center"><a href="http://localhost/AZStack/AZStackBackend/public_html/article?page=1&sortColumn=createDate&sortType=asc">Create date</a><span class=""></span></th>
                                            <th width="60px"><a href="http://localhost/AZStack/AZStackBackend/public_html/article/add">Addnew</a></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr data-article-id="70">
                                            <td class="middle" scope="row">1</td>
                                            <td class="article-title middle"><a target="_blank" href="http://localhost/AZStack/AZStackBackend/public_html/press/view/id/70">Communications Platform Layer Adds Support For Webhooks</a></td>
                                            <td class="article-description middle">The company explains via blog post that, while the Layer Platform API is used to input data into conversations, webhooks can serve as an output. “As a developer,…</td>
                                            <td class="article-image middle"><img src="https://s0.wp.com/wp-content/themes/vip/techcrunch-2013/assets/images/logo.svg" /></td>
                                            <td class="article-createDate middle" align="center">2015-12-25 15:49:01</td>
                                            <td class="middle" align="center"><a class="editarticle" href="http://localhost/AZStack/AZStackBackend/public_html/article/edit/id/70"></a>|<a class="deletearticle" data-article-id="70" href="javascript:;"></a></td>
                                        </tr>
                                        <tr data-article-id="71">
                                            <td class="middle" scope="row">2</td>
                                            <td class="article-title middle"><a target="_blank" href="http://techcrunch.com/2015/09/22/communications-platform-layer-adds-support-for-webhooks/">Communications Platform Layer Adds Support For Webhooks</a></td>
                                            <td class="article-description middle">The company explains via blog post that, while the Layer Platform API is used to input data into conversations, webhooks can serve as an output. “As a developer,…</td>
                                            <td class="article-image middle"><img src="https://s0.wp.com/wp-content/themes/vip/techcrunch-2013/assets/images/logo.svg" /></td>
                                            <td class="article-createDate middle" align="center">2015-12-25 15:49:01</td>
                                            <td class="middle" align="center"><a class="editarticle" href="http://localhost/AZStack/AZStackBackend/public_html/article/edit/id/71"></a>|<a class="deletearticle" data-article-id="71" href="javascript:;"></a></td>
                                        </tr>
                                        <tr data-article-id="72">
                                            <td class="middle" scope="row">3</td>
                                            <td class="article-title middle"><a target="_blank" href="http://techcrunch.com/2015/09/22/communications-platform-layer-adds-support-for-webhooks/">Communications Platform Layer Adds Support For Webhooks</a></td>
                                            <td class="article-description middle">The company explains via blog post that, while the Layer Platform API is used to input data into conversations, webhooks can serve as an output. “As a developer,…</td>
                                            <td class="article-image middle"><img src="https://s0.wp.com/wp-content/themes/vip/techcrunch-2013/assets/images/logo.svg" /></td>
                                            <td class="article-createDate middle" align="center">2015-12-25 15:49:01</td>
                                            <td class="middle" align="center"><a class="editarticle" href="http://localhost/AZStack/AZStackBackend/public_html/article/edit/id/72"></a>|<a class="deletearticle" data-article-id="72" href="javascript:;"></a></td>
                                        </tr>
                                        <tr data-article-id="73">
                                            <td class="middle" scope="row">4</td>
                                            <td class="article-title middle"><a target="_blank" href="http://techcrunch.com/2015/09/22/communications-platform-layer-adds-support-for-webhooks/">Communications Platform Layer Adds Support For Webhooks</a></td>
                                            <td class="article-description middle">The company explains via blog post that, while the Layer Platform API is used to input data into conversations, webhooks can serve as an output. “As a developer,…</td>
                                            <td class="article-image middle"><img src="https://s0.wp.com/wp-content/themes/vip/techcrunch-2013/assets/images/logo.svg" /></td>
                                            <td class="article-createDate middle" align="center">2015-12-25 15:49:01</td>
                                            <td class="middle" align="center"><a class="editarticle" href="http://localhost/AZStack/AZStackBackend/public_html/article/edit/id/73"></a>|<a class="deletearticle" data-article-id="73" href="javascript:;"></a></td>
                                        </tr>
                                        <tr data-article-id="74">
                                            <td class="middle" scope="row">5</td>
                                            <td class="article-title middle"><a target="_blank" href="http://techcrunch.com/2015/09/22/communications-platform-layer-adds-support-for-webhooks/">Communications Platform Layer Adds Support For Webhooks</a></td>
                                            <td class="article-description middle">The company explains via blog post that, while the Layer Platform API is used to input data into conversations, webhooks can serve as an output. “As a developer,…</td>
                                            <td class="article-image middle"><img src="https://s0.wp.com/wp-content/themes/vip/techcrunch-2013/assets/images/logo.svg" /></td>
                                            <td class="article-createDate middle" align="center">2015-12-25 15:49:01</td>
                                            <td class="middle" align="center"><a class="editarticle" href="http://localhost/AZStack/AZStackBackend/public_html/article/edit/id/74"></a>|<a class="deletearticle" data-article-id="74" href="javascript:;"></a></td>
                                        </tr>
                                        <tr data-article-id="75">
                                            <td class="middle" scope="row">6</td>
                                            <td class="article-title middle"><a target="_blank" href="http://techcrunch.com/2015/09/22/communications-platform-layer-adds-support-for-webhooks/">Communications Platform Layer Adds Support For Webhooks</a></td>
                                            <td class="article-description middle">The company explains via blog post that, while the Layer Platform API is used to input data into conversations, webhooks can serve as an output. “As a developer,…</td>
                                            <td class="article-image middle"><img src="https://s0.wp.com/wp-content/themes/vip/techcrunch-2013/assets/images/logo.svg" /></td>
                                            <td class="article-createDate middle" align="center">2015-12-25 15:49:01</td>
                                            <td class="middle" align="center"><a class="editarticle" href="http://localhost/AZStack/AZStackBackend/public_html/article/edit/id/75"></a>|<a class="deletearticle" data-article-id="75" href="javascript:;"></a></td>
                                        </tr>
                                        <tr data-article-id="76">
                                            <td class="middle" scope="row">7</td>
                                            <td class="article-title middle"><a target="_blank" href="http://techcrunch.com/2015/09/22/communications-platform-layer-adds-support-for-webhooks/">Communications Platform Layer Adds Support For Webhooks</a></td>
                                            <td class="article-description middle">The company explains via blog post that, while the Layer Platform API is used to input data into conversations, webhooks can serve as an output. “As a developer,…</td>
                                            <td class="article-image middle"><img src="https://s0.wp.com/wp-content/themes/vip/techcrunch-2013/assets/images/logo.svg" /></td>
                                            <td class="article-createDate middle" align="center">2015-12-25 15:49:01</td>
                                            <td class="middle" align="center"><a class="editarticle" href="http://localhost/AZStack/AZStackBackend/public_html/article/edit/id/76"></a>|<a class="deletearticle" data-article-id="76" href="javascript:;"></a></td>
                                        </tr>
                                        <tr data-article-id="77">
                                            <td class="middle" scope="row">8</td>
                                            <td class="article-title middle"><a target="_blank" href="http://techcrunch.com/2015/09/22/communications-platform-layer-adds-support-for-webhooks/">Communications Platform Layer Adds Support For Webhooks</a></td>
                                            <td class="article-description middle">The company explains via blog post that, while the Layer Platform API is used to input data into conversations, webhooks can serve as an output. “As a developer,…</td>
                                            <td class="article-image middle"><img src="https://s0.wp.com/wp-content/themes/vip/techcrunch-2013/assets/images/logo.svg" /></td>
                                            <td class="article-createDate middle" align="center">2015-12-25 15:49:01</td>
                                            <td class="middle" align="center"><a class="editarticle" href="http://localhost/AZStack/AZStackBackend/public_html/article/edit/id/77"></a>|<a class="deletearticle" data-article-id="77" href="javascript:;"></a></td>
                                        </tr>
                                        <tr data-article-id="78">
                                            <td class="middle" scope="row">9</td>
                                            <td class="article-title middle"><a target="_blank" href="http://techcrunch.com/2015/09/22/communications-platform-layer-adds-support-for-webhooks/">Communications Platform Layer Adds Support For Webhooks</a></td>
                                            <td class="article-description middle">The company explains via blog post that, while the Layer Platform API is used to input data into conversations, webhooks can serve as an output. “As a developer,…</td>
                                            <td class="article-image middle"><img src="https://s0.wp.com/wp-content/themes/vip/techcrunch-2013/assets/images/logo.svg" /></td>
                                            <td class="article-createDate middle" align="center">2015-12-25 15:49:01</td>
                                            <td class="middle" align="center"><a class="editarticle" href="http://localhost/AZStack/AZStackBackend/public_html/article/edit/id/78"></a>|<a class="deletearticle" data-article-id="78" href="javascript:;"></a></td>
                                        </tr>
                                        <tr data-article-id="79">
                                            <td class="middle" scope="row">10</td>
                                            <td class="article-title middle"><a target="_blank" href="http://techcrunch.com/2015/09/22/communications-platform-layer-adds-support-for-webhooks/">Communications Platform Layer Adds Support For Webhooks</a></td>
                                            <td class="article-description middle">The company explains via blog post that, while the Layer Platform API is used to input data into conversations, webhooks can serve as an output. “As a developer,…</td>
                                            <td class="article-image middle"><img src="https://s0.wp.com/wp-content/themes/vip/techcrunch-2013/assets/images/logo.svg" /></td>
                                            <td class="article-createDate middle" align="center">2015-12-25 15:49:01</td>
                                            <td class="middle" align="center"><a class="editarticle" href="http://localhost/AZStack/AZStackBackend/public_html/article/edit/id/79"></a>|<a class="deletearticle" data-article-id="79" href="javascript:;"></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="c-gray-box center">
                                    <ul class="pagination">
                                        <li class="hide"><a href="http://localhost/AZStack/AZStackBackend/public_html/article?page=0&sortColumn=&sortType=">&laquo;</a></li>
                                        <li class='active'><a href='http://localhost/AZStack/AZStackBackend/public_html/article?page=1&sortColumn=&sortType='>1</a></li><li class=''><a href='http://localhost/AZStack/AZStackBackend/public_html/article?page=2&sortColumn=&sortType='>2</a></li><li class=''><a href='http://localhost/AZStack/AZStackBackend/public_html/article?page=3&sortColumn=&sortType='>3</a></li><li class=''><a href='http://localhost/AZStack/AZStackBackend/public_html/article?page=4&sortColumn=&sortType='>4</a></li><li class=''><a href='http://localhost/AZStack/AZStackBackend/public_html/article?page=5&sortColumn=&sortType='>5</a></li><li class=''><a href='http://localhost/AZStack/AZStackBackend/public_html/article?page=6&sortColumn=&sortType='>6</a></li>            <li class=""><a href="http://localhost/AZStack/AZStackBackend/public_html/article?page=2&sortColumn=&sortType=">&raquo;</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="modal-wrapper hide">
                                <div>
                                    <div class="new-modal delete-confirm">
                                        <div>
                                            <a href="javascript:;" class="hide" title="Close"></a>
                                            <fieldset>
                                                <h4>Are you sure you want to <b>delete</b> this article?</h4>
                                                <span class="confirm-sub"></span>
                                                <p><b>Deleting a article cannot be undone.</b></p>
                                                <button class="delete btn-red">Permanently delete this article</button>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <form id="deleteaction" method="post" action="http://localhost/AZStack/AZStackBackend/public_html/article/delete">
                                <input type="hidden" name="id" value="" />
                                <input type="hidden" name="currentUrl" value="http://localhost/AZStack/AZStackBackend/public_html/article" />
                            </form>
                            <script type='text/javascript'>
                                $(document).ready(function() {
                                    $(document).on('click', "a[class='hide']", function() {
                                        $('.modal-wrapper').addClass("hide");
                                    });
                                    $(document).on('click', ".btn-red", function() {
                                        $("#deleteaction").submit();
                                    });
                                    $(document).on('click', ".deletearticle", function() {
                                        var articleId = $(this).attr("data-article-id");
                                        $("#deleteaction input[name=id]").val(articleId);
                                        $(".confirm-sub").text($("tr[data-article-id=" + articleId + "] .article-title").text());
                                        $('.modal-wrapper').removeClass("hide");
                                        return false;
                                    });
                                });
                            </script>                        </div>
                        <div class="main-content-full"></div>
                    </main>
                </div>
                <div id="content-spacer-bottom"></div>
            </div>
            <footer>
                <ul>
                    <li class="copyright">© 2016 AZStack Pte. Ltd.</li>
                </ul>
                <ul class="right">
                    <li><a data-bypass href="http://developer.azstack.co/terms">Terms</a></li>
                    <li><a data-bypass href="http://developer.azstack.co/press">Press</a></li>
                </ul>
            </footer>
        </div>
    </body>
</html>