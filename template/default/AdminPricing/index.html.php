<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminPricing">Pricing</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Pricing</li>
        </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        $cssClass = "active";
                        foreach($this->languageAll as $language):
                            echo '<li class="'. $cssClass .'"><a href="#lang_pricing_list_'. $language .'" data-toggle="tab">'. $language .'</a></li>';
                            $cssClass = "";
                        endforeach;
                    ?>
                </ul>
                <form method="post">
                    <div class="tab-content">
                            <?php
                                $cssClass = "active";
                                foreach($this->languageAll as $language):?>
                                    <div class="tab-pane <?= $cssClass ?>" id="lang_pricing_list_<?= $language ?>">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label>Text Header</label>
                                                <input value="<?= $this->header_pricing[$language] ?>" name="header-pricing[<?= $language ?>]" type="text" class="form-control" placeholder="Text Header ..." />
                                            </div>
                                            <div class="form-group">
                                                <label>Text Header 2</label>
                                                <input value="<?= $this->azstack_flatform[$language] ?>" name="azstack-flatform[<?= $language ?>]" type="text" class="form-control" placeholder="Text Header 2 ..." />
                                            </div>
                                        </div><!-- /.box-body -->
                                        <div class="box-footer">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                        <div class="box-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                  <tr>
                                                    <th style="width: 5%">Id</th>
                                                    <th style="width: 15%">Name</th>
                                                    <th style="width: 10%">Currency Unit</th>
                                                    <th style="width: 5%">Price</th>
                                                    <th style="width: 10%">Text Per Month</th>
                                                    <th style="width: 25%">Description</th>
                                                    <th style="width: 25%">Text Bottom</th>
                                                    <th style="width: 5%">Action</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        foreach($this->pricing_list[$language] as $item):?>
                                                            <tr>
                                                                <td><?= $item['id'] ?></td>
                                                                <td><?= $item['typeName'] ?></td>
                                                                <td><?= $item['currency_unit'] ?></td>
                                                                <td><?= $item['price'] ?></td>
                                                                <td><?= $item['per_month'] ?></td>
                                                                <td><?= $item['quality'] ?></td>
                                                                <td><?= $item['content_bottom'] ?></td>
                                                                <td><a href="<?= BASE_URL ?>AdminPricing/edit/id/<?= $item['id'] ?>/language/<?= $language ?>">Edit</a></td>
                                                              </tr>
                                                        <?php endforeach;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div><!-- /.box-body -->
                                    </div><!-- /.tab-pane -->
                                    
                                    <?php $cssClass = "";
                                endforeach;
                            ?>
                    </div><!-- /.tab-content -->
                </form>
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->