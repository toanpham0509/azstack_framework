<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminPricing">Pricing</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Pricing</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form method="post" role="form">
                <div class="box-body">
                    <input type="hidden" name="icon" value="<?= $this->pricing['icon'] ?>" />
                    <input type="hidden" name="cssClass" value="<?= $this->pricing['cssClass'] ?>" />
                    <div class="form-group">
                        <label>Pricing name</label>
                        <input value="<?= $this->pricing['typeName'] ?>" name="typeName" type="text" class="form-control" placeholder="Pricing name ..." />
                    </div>
                    <div class="form-group">
                        <label>Currency unit</label>
                        <input value="<?= $this->pricing['currency_unit'] ?>" name="currency_unit" type="text" class="form-control" placeholder="Currency unit ..." />
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        <input value="<?= $this->pricing['price'] ?>" name="price" type="text" class="form-control" placeholder="Price ..." />
                    </div>
                    <div class="form-group">
                        <label>Text per month</label>
                        <input value="<?= $this->pricing['per_month'] ?>" name="per_month" type="text" class="form-control" placeholder="Text per month ..." />
                    </div>
                    <div class="form-group">
                        <label>Quality</label>
                        <input value="<?= $this->pricing['quality'] ?>" name="quality" type="text" class="form-control" placeholder="Quality ..." />
                    </div>
                    <div class="form-group">
                        <label>Text bottom</label>
                        <input value="<?= $this->pricing['content_bottom'] ?>" name="content_bottom" type="text" class="form-control" placeholder="Text bottom ..." />
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <textarea class="ckeditor" name="content" id="content" class="form-control"><?= $this->pricing['content'] ?></textarea>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->