<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminReassurance">Reassurance</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
        <li class="active">Reassurance</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form method="post" role="form">
                <div class="box-body">

                    <input type="hidden" name="type" value="<?= $this->reassurance['type'] ?>" />
                    <div class="form-group">
                        <label>Icon</label>
                        <input value="<?= $this->reassurance['icon'] ?>" id="icon" name="icon" type="text" class="form-control" placeholder="Icon ..." />
                        <button onclick="browerCKfinder('icon')" style="top: 35px; right: 25px; position: absolute" type="button" class="btn btn-primary">Browser</button>
                    </div>
                    <div class="form-group">
                        <label>Link</label>
                        <input value="<?= $this->reassurance['link'] ?>" name="link" type="text" class="form-control" placeholder="Link ..." />
                    </div>

                </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->
<script>
    function browerCKfinder(txtimage) {
        var ck = new CKFinder();
        if (document.getElementById(txtimage).value === "") {
            ck.startupPath = "doi-tac:/";
        } else {
            ck.startupPath = "doi-tac:" + document.getElementById(txtimage).value.substring("uploads/images/".length);
        }
        ck.selectActionFunction = function(fileUrl) {
            document.getElementById(txtimage).value = fileUrl;
        };
        ck.popup();
    }
</script>