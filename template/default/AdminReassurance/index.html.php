<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminReassurance">Reassurance</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Reassurance</li>
        </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-6">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        $cssClass = "active";
                        foreach($this->languageAll as $language):
                            echo '<li class="'. $cssClass .'"><a href="#lang_trusted_by_'. $language .'" data-toggle="tab">'. $language .'</a></li>';
                            $cssClass = "";
                        endforeach;
                    ?>
                </ul>
                <form method="post">
                    <div class="tab-content">
                            <?php
                                $cssClass = "active";
                                foreach($this->languageAll as $language):?>
                                    <div class="tab-pane <?= $cssClass ?>" id="lang_trusted_by_<?= $language ?>">
                                        <div class="box-body">
                                          <div class="form-group">
                                            <input name="trusted-by[<?= $language ?>]" value="<?= $this->trusted_by[$language] ?>" type="text" class="form-control">
                                          </div>
                                        </div><!-- /.box-body -->
                                    </div><!-- /.tab-pane -->
                                    <?php $cssClass = "";
                                endforeach;
                            ?>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </div><!-- /.tab-content -->
                </form>
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style="width: 10%;">Id</th>
                        <th style="width: 20%;">Icon</th>
                        <th>Link</th>
                        <th style="width: 18%;"><a href="<?= BASE_URL ?>AdminReassurance/addnew?type=trusted-by">Add new</a></th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($this->trusted_by_list as $item):?>
                                <tr>
                                    <td><?= $item['id'] ?></td>
                                    <td><img src="<?= STATIC_URL . $item['icon'] ?>" /></td>
                                    <td><a href="<?= $item['link'] ?>"><?= $item['link'] ?></a></td>
                                    <td><a href="<?= BASE_URL ?>AdminReassurance/edit/id/<?= $item['id'] ?>">Edit</a> | <a href="<?= BASE_URL ?>AdminReassurance/delete/id/<?= $item['id'] ?>">Delete</a></td>
                                  </tr>
                            <?php endforeach;
                        ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
            <div class="col-md-6">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        $cssClass = "active";
                        foreach($this->languageAll as $language):
                            echo '<li class="'. $cssClass .'"><a href="#lang_partners_support_'. $language .'" data-toggle="tab">'. $language .'</a></li>';
                            $cssClass = "";
                        endforeach;
                    ?>
                </ul>
                <form method="post">
                    <div class="tab-content">
                            <?php
                                $cssClass = "active";
                                foreach($this->languageAll as $language):?>
                                    <div class="tab-pane <?= $cssClass ?>" id="lang_partners_support_<?= $language ?>">
                                        <div class="box-body">
                                          <div class="form-group">
                                            <input name="partners-support[<?= $language ?>]" value="<?= $this->partners_support[$language] ?>" type="text" class="form-control">
                                          </div>
                                        </div><!-- /.box-body -->
                                    </div><!-- /.tab-pane -->
                                    <?php $cssClass = "";
                                endforeach;
                            ?>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </div><!-- /.tab-content -->
                </form>
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style="width: 10%;">Id</th>
                        <th style="width: 20%;">Icon</th>
                        <th>Link</th>
                        <th style="width: 18%;"><a href="<?= BASE_URL ?>AdminReassurance/addnew?type=partners-support">Add new</a></th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($this->partners_support_list as $item):?>
                                <tr>
                                    <td><?= $item['id'] ?></td>
                                    <td><img src="<?= STATIC_URL . $item['icon'] ?>" /></td>
                                    <td><a href="<?= $item['link'] ?>"><?= $item['link'] ?></a></td>
                                    <td><a href="<?= BASE_URL ?>AdminReassurance/edit/id/<?= $item['id'] ?>">Edit</a> | <a href="<?= BASE_URL ?>AdminReassurance/delete/id/<?= $item['id'] ?>">Delete</a></td>
                                  </tr>
                            <?php endforeach;
                        ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->