<section class="content-header">
    <h1>
        <?= $this->pageName ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>Admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="<?= BASE_URL ?>/AdminLayoutSlider">Posts</a></li>
        <li class="active"><?= $this->pageName ?></li>
    </ol>
    <br />
    <a href="<?= BASE_URL ?>AdminPost" class="btn btn-danger">Back</a>
</section>
<section class="content">
    <?php if(strlen($this->message)) : ?>
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <?= $this->message ?>
        </div>
    <?php endif; ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?= $this->pageName; ?>
            </h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post" action="">
            <div class="box-body">
                <div class="form-group">
                    <label for="editor1">Language</label>
                    <input disabled="disabled" type="text" name="language" class="form-control" value="<?=
                         $this->post['language']
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="title">Post title</label>
                    <input required="required" type="text" id="title" name="title" class="form-control" value="<?=
                    $this->post['title']
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="url">Post url</label>
                    <input type="text" id="url" name="url" class="form-control" value="<?=
                    $this->post['url']
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="url">Post Category</label>
                    <select class="form-control" name="categoryId">
                        <?php
                        if(!empty($this->categories)) {
                            foreach($this->categories as $category) {
                                ?>
                                <option <?php
                                    if($category['categoryId'] == $this->post['categoryId'])
                                        echo "selected='selected'";
                                ?> value="<?= $category['categoryId'] ?>">
                                    <?= $category['categoryName'] ?>
                                </option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="excerpt">Excerpt</label>
                        <textarea id="excerpt" class="ckeditor" name="excerpt" class="form-control"><?=
                            $this->post['excerpt']
                            ?></textarea>
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                        <textarea id="content" class="ckeditor" name="content" class="form-control"><?=
                            $this->post['content']
                            ?></textarea>
                </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
                <button name="submit" type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</section>