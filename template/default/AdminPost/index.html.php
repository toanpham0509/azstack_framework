<section class="content-header">
    <h1>
        <?= $this->pageName ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>Admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?= $this->pageName ?></li>
    </ol>
    <br />
    <a href="<?= BASE_URL ?>AdminPost/addNew" class="btn btn-danger">Add new post</a>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?= $this->pageName ?>
            </h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px">#</th>
                    <th>Title</th>
                    <th>Url</th>
                    <th>Category</th>
                    <th>Create Time</th>
                    <th>Last Update</th>
                    <th>Language</th>
                    <th style="width: 40px">Action</th>
                </tr>
                <?php
                if(isset($this->posts) && !empty($this->posts)) {
                    $i = 1;
                    foreach ($this->posts as $post) {
                        ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $post['title'] ?></td>
                            <td><?= $post['url'] ?></td>
                            <td><a href="<?= BASE_URL ?>AdminPostCategory/edit?categoryId=<?= $post['categoryId'] ?>">
                                <?= $post['categoryName'] ?></a></td>
                            <td><?php
                                if($post['createTime'] > 0)
                                    echo date("H:i:s d/m/Y", $post['createTime']);
                                ?></td>
                            <td><?php
                                if($post['lastUpdate'] > 0)
                                    echo date("H:i:s d/m/Y", $post['lastUpdate']);
                                ?></td>
                            <td><?= $post['language'] ?></td>
                            <td>
                                <a href="<?=
                                    BASE_URL
                                    . "AdminPost/edit?postId="
                                    . $post['id'] ?>">Edit</a>
                                <a onclick="return confirm('Are you sure to want delete this item?')" href="<?=
                                    BASE_URL
                                    . "AdminPost/delete?postId="
                                    . $post['id'] ?>">Delete</a>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>