<section class="content-header">
    <h1>
        Slider management - All language
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>Admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Layout options</li>
    </ol>
</section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Slider management - All language
                </h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <a href="<?= BASE_URL ?>AdminLayoutSlider/addNew" class="btn btn-danger">Add new language</a>
                <br /><br />
                <table class="table table-bordered">
                    <tbody><tr>
                        <th style="width: 10px">#</th>
                        <th>Language</th>
                        <th style="width: 40px">Action</th>
                    </tr>
                    <?php
                        if(isset($this->sliders) && !empty($this->sliders)) {
                            $i = 1;
                            foreach ($this->sliders as $slider) {
                                ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= $slider['language'] ?></td>
                                    <td>
                                        <a href="<?=
                                            BASE_URL
                                            . "AdminLayoutSlider/update?lang="
                                            . $slider['language'] ?>">Edit</a>
                                        <a href="<?=
                                            BASE_URL . "AdminLayoutSlider/delete?lang="
                                            . $slider['language']
                                        ?>" onclick="return confirm('Are you sure to want delete this item?')">Delete</a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>