<section class="content-header">
    <h1>
        Slider management - <?= $this->language ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>Admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="<?= BASE_URL ?>/AdminLayoutSlider">Layout options</a></li>
        <li class="active"><?= $this->language ?></li>
    </ol>
</section>
    <section class="content">
        <br />
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    Edit Slider <?= $this->language; ?>
                </h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="">
                <div class="box-body">
                    <div class="text-danger"><b><?= $this->message; ?></b></div>
                    <div class="form-group">
                        <label for="editor1">Language</label>
                        <input disabled="disabled" type="text" name="language" class="form-control" value="<?=
                            $this->language
                        ?>" />
                    </div>
                    <div class="form-group">
                        <label for="editor1">Header slider</label>
                        <textarea style="min-height: 120px" id="editor1" class="ckeditor" placeholder="Header slider..." name="headerSlider" class="form-control"><?=
                                $this->headerSlider;
                            ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="editor2">Slider list</label>
                        <textarea style="min-height: 120px" id="editor2" class="ckeditor" placeholder="Slider list" name="sliderList" class="form-control"><?=
                                $this->sliderList;
                            ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="getStartedText">Get Started button text</label>
                        <input required="required" type="text" id="getStartedText" name="getStartedText" class="form-control" value="<?=
                        $this->getStartedText
                        ?>" />
                    </div>
                    <div class="form-group">
                        <label for="getStartedLink">Get Started button link</label>
                        <input type="text" id="getStartedLink" name="getStartedLink" class="form-control" value="<?=
                        $this->getStartedLink
                        ?>" />
                    </div>
                    <div class="form-group">
                        <label for="howItWorkText">How it work text</label>
                        <input required="required" type="text" id="howItWorkText" name="howItWorkText" class="form-control" value="<?=
                        $this->howItWorkText
                        ?>" />
                    </div>
                    <div class="form-group">
                        <label for="howItWorkLink">How it work link</label>
                        <input type="text" id="howItWorkLink" name="howItWorkLink" class="form-control" value="<?=
                        $this->howItWorkLink
                        ?>" />
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                    <a href="<?= BASE_URL ?>AdminLayoutSlider" class="btn btn-primary">Close</a>
                </div>
            </form>
        </div>
    </section>