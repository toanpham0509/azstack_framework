<div class="login-box">
    <div class="login-logo">
        <a href="<?= BASE_URL ?>AdminLogin"><b>Admin</b> AzStack</a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <?php if(strlen($this->errorMessage)) : ?>
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <?= $this->errorMessage; ?>
        </div>
        <?php endif; ?>
        <form action="" method="post">
            <div class="form-group has-feedback">
                <input
                    name="email"
                    type="email"
                    class="form-control"
                    placeholder="Email"
                    value="<?= $this->dataEmail ?>" />
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input
                    name="password"
                    type="password"
                    class="form-control"
                    placeholder="Password"
                     />
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <!--
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> Remember Me
                        </label>
                    </div>
                    -->
                </div><!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" name="submit" class="btn btn-danger btn-block btn-flat">Sign In</button>
                </div><!-- /.col -->
            </div>
        </form>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->