<section class="content-header">
    <h1>
        <?= $this->pageName . " -> Edit language"; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>Admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?= $this->pageName; ?></li>
    </ol>
</section>
<section class="content">
    <br />
    <div class="box box-primary">
        <div class="box-header with-border">
            Edit language
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post" action="">
            <div class="box-body">
                <div class="text-danger"><b><?= $this->message; ?></b></div>
                <div class="form-group">
                    <label for="editor1">Language</label>
                    <input disabled="disabled" required="required" type="text" name="language" class="form-control" value="<?=
                    $this->language
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="editor1">Header</label>
                        <textarea id="editor1" class="ckeditor" placeholder="Header platform..." name="headerPlatform" class="form-control"><?=
                            $this->headerPlatform;
                            ?></textarea>
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input id="image" required="required" type="text" name="image" class="form-control" value="<?=
                    $this->image
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="textReadMore">Text read more</label>
                    <input id="textReadMore" required="required" type="text" name="textReadMore" class="form-control" value="<?=
                        $this->textReadMore
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="readMore">Link read more</label>
                    <input id="readMore" required="required" type="text" name="readMore" class="form-control" value="<?=
                    $this->readMore
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="keyHeaderDownload">Description</label>
                        <textarea id="editor2" class="ckeditor" placeholder="Description..." name="headerDownload" class="form-control"><?=
                            $this->headerDownload;
                            ?></textarea>
                </div>
                <div class="form-group">
                    <label for="textAndroid">Text button android</label>
                    <input id="textAndroid" required="required" type="text" name="textAndroid" class="form-control" value="<?=
                    $this->textAndroid
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="linkAndroid">Link android</label>
                    <input id="linkAndroid" type="text" name="linkAndroid" class="form-control" value="<?=
                    $this->linkAndroid
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="textIos">Text button ios</label>
                    <input id="textIos" required="required" type="text" name="textIos" class="form-control" value="<?=
                    $this->textIos
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="linkIos">Link ios</label>
                    <input id="linkIos" type="text" name="linkIos" class="form-control" value="<?=
                    $this->linkIos
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="textGitHub">Text button GitHub</label>
                    <input id="textGitHub" required="required" type="text" name="textGitHub" class="form-control" value="<?=
                    $this->textGitHub
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="linkGitHub">Link GitHub</label>
                    <input id="linkGitHub" type="text" name="linkGitHub" class="form-control" value="<?=
                    $this->linkGitHub
                    ?>" />
                </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
                <button  name="submit" type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</section>