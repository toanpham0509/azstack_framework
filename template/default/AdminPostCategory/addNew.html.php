<section class="content-header">
    <h1>
        <?= $this->pageName ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>Admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="<?= BASE_URL ?>/AdminLayoutSlider">Layout options</a></li>
        <li class="active"><?= $this->pageName ?></li>
    </ol>
    <br />
    <a href="<?= BASE_URL ?>AdminPostCategory/categories?lang=<?= $this->language ?>" class="btn btn-danger">Back</a>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?= $this->pageName; ?>
            </h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="post" action="">
            <div class="box-body">
                <div class="text-danger"><b><?= $this->message; ?></b></div>
                <div class="form-group">
                    <label for="editor1">Language</label>
                    <input disabled="disabled" type="text" name="language" class="form-control" value="<?=
                    $this->language
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="categoryName">Category name</label>
                    <input required="required" type="text" id="categoryName" name="categoryName" class="form-control" value="<?=
                    $this->category['categoryName']
                    ?>" />
                </div>
                <div class="form-group">
                    <label for="categoryUrl">Category url</label>
                    <input type="text" id="categoryUrl" name="categoryUrl" class="form-control" value="<?=
                    urldecode($this->category['categoryUrl'])
                    ?>" />
                </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
                <button name="submit" type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</section>