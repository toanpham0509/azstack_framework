<section class="content-header">
    <h1>
        <?= $this->pageName ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>Admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?= $this->pageName ?></li>
    </ol>
    <br />
    <a href="<?= BASE_URL ?>AdminPostCategory" class="btn btn-danger">Back</a>
    <a href="<?= BASE_URL ?>AdminPostCategory/addNew?lang=<?= $this->language ?>" class="btn btn-danger">Add new post</a>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?= $this->pageName ?>
            </h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px">#</th>
                    <th>Category name</th>
                    <th>Category url</th>
                    <th>Language</th>
                    <th>Create time</th>
                    <th>Last Update</th>
                    <th style="width: 40px">Action</th>
                </tr>
                <?php
                if(isset($this->categories) && !empty($this->categories)) {
                    $i = 1;
                    foreach ($this->categories as $category) {
                        ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $category['categoryName'] ?></td>
                            <td><?= $category['categoryUrl'] ?></td>
                            <td><?= $category['language'] ?></td>
                            <td><?php if($category['createTime'] > 0)
                                    echo date("H:i:s, d/M/Y", $category['createTime']); ?></td>
                            <td><?php if($category['lastUpdate'] > 0)
                                    echo date("H:i:s, d/M/Y", $category['lastUpdate']); ?></td>
                            <td>
                                <a href="<?=
                                    BASE_URL
                                    . "AdminPostCategory/edit?categoryId="
                                    . $category['categoryId'] ?>">
                                    Edit
                                </a>
                                <a onclick="return confirm('Are you sure to want delete this item?')" href="<?=
                                    BASE_URL
                                    . "AdminPostCategory/delete?categoryId="
                                    . $category['categoryId'] ?>">
                                    Delete
                                </a>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>