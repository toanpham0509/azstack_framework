<section class="content-header">
    <h1>
        <?= $this->pageName ?>: All language
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>Admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?= $this->pageName ?></li>
    </ol>
</section>
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">
                <?= $this->pageName ?>: All language
            </h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px">#</th>
                    <th>Language</th>
                    <th style="width: 40px">Action</th>
                </tr>
                <?php
                if(isset($this->languages) && !empty($this->languages)) {
                    $i = 1;
                    foreach ($this->languages as $slider) {
                        ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $slider ?></td>
                            <td>
                                <a href="<?=
                                BASE_URL
                                . "AdminPostCategory/categories?lang="
                                . $slider ?>">Show categories</a>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>