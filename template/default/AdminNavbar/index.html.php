<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <a href="<?= BASE_URL ?>AdminNavbar">Navbar</a>
        <small>Manager page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= BASE_URL ?>admin"><i class="fa fa-dashboard"></i> Admin</a></li>
            <li class="active">Navbar</li>
        </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        $cssClass = "active";
                        foreach($this->languageAll as $language):
                            echo '<li class="'. $cssClass .'"><a href="#lang_navbar_list_'. $language .'" data-toggle="tab">'. $language .'</a></li>';
                            $cssClass = "";
                        endforeach;
                    ?>
                </ul>
                <form method="post">
                    <div class="tab-content">
                        <?php
                            $cssClass = "active";
                            foreach($this->languageAll as $language):?>
                                <div class="tab-pane <?= $cssClass ?>" id="lang_navbar_list_<?= $language ?>">
                                    <div class="box-body">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input value="<?= $this->features[$language]['value'] ?>" name="features[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->features[$language]['value'] ?> ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input value="<?= $this->features[$language]['link'] ?>" name="features[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input value="<?= $this->free_download[$language]['value'] ?>" name="free-download[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->free_download[$language]['value'] ?> ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input value="<?= $this->free_download[$language]['link'] ?>" name="free-download[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input value="<?= $this->about_us[$language]['value'] ?>" name="about-us[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->about_us[$language]['value'] ?> ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input value="<?= $this->about_us[$language]['link'] ?>" name="about-us[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input value="<?= $this->pricing[$language]['value'] ?>" name="pricing[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->pricing[$language]['value'] ?> ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input value="<?= $this->pricing[$language]['link'] ?>" name="pricing[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input value="<?= $this->login[$language]['value'] ?>" name="login[<?= $language ?>][value]" type="text" class="form-control" placeholder="<?= $this->login[$language]['value'] ?> ..." />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input value="<?= $this->login[$language]['link'] ?>" name="login[<?= $language ?>][link]" type="text" class="form-control" placeholder="Link ..." />
                                            </div>
                                        </div>
                                    </div><!-- /.box-body -->
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div><!-- /.tab-pane -->

                                <?php $cssClass = "";
                            endforeach;
                        ?>
                    </div><!-- /.tab-content -->
                </form>
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->
    </div> <!-- /.row -->
</section><!-- /.content -->