<?php

/**
 * @package		Borua
 * @copyright   Copyright (c) 2010 Anhxtanh3087
 * @author      Dau Ngoc Huy - Anhxtanh3087 - huydaungoc@gmail.com
 * @since       May 13, 2011
 * @version     $Id: Cache_MySQL 1 May 13, 2011 12:19:27 PM Anhxtanh3087 $
 */
class Cache_MySQL /*extends Cache*/ {

	public static $dhCache;
	private $_table = 'cache';
	private $_tableTag = 'cache_tag';

	public static function getInstance() {
		if (self::$dhCache == null) {
			self::$dhCache = new Cache_MySQL();
		}
		return self::$dhCache;
	}

	public function __construct() {
		$this->_table = new Db_TableAbstract('cache');
		$this->_tableTag = new Db_TableAbstract('cache_tag');
	}

	public function load($cacheId, $testCacheValidity = true) {
		$sql = 'SELECT data FROM ' . $this->_table->name . " WHERE cacheId='$cacheId'";
		if ($testCacheValidity) {
			$sql = $sql . " AND (expire=0 OR expire>" . time() . ')';
		}
		$row = $this->_table->fetchRow($sql);
		if ($row) {
			return unserialize($row->data);
		}
		return false;
	}

	public function test($cacheId) {
		$sql = 'SELECT modified FROM ' . $this->_table->name . " WHERE cacheId='$cacheId' AND (expire=0 OR expire>" . time() . ')';
		$row = $this->_table->fetchRow($sql);
		if ($row) {
			return ((int) $row->modified);
		}
		return false;
	}

	public function save($data, $cacheId, $tags = array(), $expire=0) {
		$this->_table->delete('cacheId=\'' . $cacheId . '\'');
		$d['data'] = serialize($data);
		$d['cacheId'] = $cacheId;
		$d['modified'] = time();
		if ($expire) {
			$d['expire'] = $d['modified'] + $expire;
		} else {
			$d['expire'] = 0;
		}
		$this->_table->insert($d);
		foreach ($tags as $tag) {
			$this->_registerTag($cacheId, $tag);
		}
		return true;
	}

	private function _registerTag($cacheId, $tag) {
		$this->_tableTag->delete("cacheId='$cacheId' AND name='$tag'");
		$d['name'] = $tag;
		$d['cacheId'] = $cacheId;
		$this->_tableTag->insert($d);
	}

	public function remove($cacheId) {
		$this->_tableTag->delete("cacheId='$cacheId'");
		$this->_table->delete("cacheId='$cacheId'");
	}

	public function clean($mode, $tags = array()) {
		switch ($mode) {
			case Cache::CLEANING_MODE_ALL:
				$this->_tableTag->delete("1");
				$this->_table->delete("1");
				break;
			case Cache::CLEANING_MODE_OLD:
				$mktime = time();
				$this->_tableTag->delete("cacheId IN (SELECT cacheId FROM dh_cache WHERE expire>0 AND expire<=$mktime)");
				$this->_table->delete("expire>0 AND expire<=$mktime");
				break;
			case Cache::CLEANING_MODE_MATCHING_TAG:
				$ids = $this->getIdsMatchingTags($tags);
				foreach ($ids as $id) {
					$this->remove($id);
				}
				break;
			case Cache::CLEANING_MODE_NOT_MATCHING_TAG:
				break;
			case Cache::CLEANING_MODE_MATCHING_ANY_TAG:
				break;
			default:
				break;
		}
		return true;
	}

	private function getIdsMatchingTags($tags = array()) {
		$ids = array();
		foreach ($tags as $tag) {
			$res = $this->_tableTag->fetchAll("SELECT DISTINCT(cacheId) AS id FROM dh_cache_tag WHERE name='$tag'");
			foreach ($res as $r) {
				$ids[] = $r->id;
			}
		}
		return $ids;
	}

	private function getIdsNotMatchingTags($tags = array()) {
		
	}

}
