<?php

/**
 * @package     Borua Framework
 * @copyright   Copyright (c) 2010 Anhxtanh3087
 * @author      Dau Ngoc Huy - Anhxtanh3087 - huydaungoc@gmail.com
 * @since       Oct 1, 2010
 * @version     $Id: View.php 1 Oct 1, 2010 10:12:46 PM Anhxtanh3087 $
 */
class View {

	/**
	 * @var array('frontend_template_path','backend_template_path')
	 */
	public static $path;
	public static $engine;

	public function __construct() {
		
	}

	public function __set($key, $val) {
		$this->$key = $val;
	}

	public function __get($name) {
		return $this->$name;
	}

	/**
	 * @param String $name Tên file muốn render, ví dụ: User/test.html.php
	 * @param String $cacheId
	 * @return String Nội dung
	 */
	public function render($name, $cacheId = null, $cacheLifeTime = null) {
		$data = FALSE;
		global $cache;
		if ($cacheId && $cache) {
			$data = $cache->load(TPL_NAME . '_' . $cacheId);
		}

		if (!$data) {
			ob_start();
			$filePath = self::$path[Dispatcher::$isBackend] . $name;
			if (is_readable($filePath)) {
				include ($filePath);
			} else {
				global $config;
				include (ROOT_PATH . 'template/' . $config['template'] . '/' . $name);
			}
			$data = ob_get_contents();
			ob_end_clean();

			if ($cacheId && $cache) {
				if (!$cacheLifeTime) {
					$cache->save($data, TPL_NAME . '_' . $cacheId);
				} else {
					$cache->save($data, TPL_NAME . '_' . $cacheId, null, $cacheLifeTime);
				}
			}
		}

		return $data;
	}

	/**
	 * @param String $filePath đường dẫn đầy đủ file muốn render
	 * @param String $cacheId
	 * @return String Nội dung
	 */
	public function fetch($filePath, $cacheId = null) {
		ob_start();
		include ($filePath);
		$data = ob_get_contents();
		ob_end_clean();

		return $data;
	}

	public function clearCache($name) {
		return self::$engine->clear_cache($name);
	}

	public function clearAllCache() {
		return self::$engine->clear_all_cache();
	}

	/**
	 * @param String $dateTime Ex: 2010-09-23 00:28:25
	 * @return String
	 */
	/*
	  public function time($dateTime1) {
	  $dateTime = strtotime($dateTime1);
	  $node ['date'] = date('d/m/Y', $dateTime);
	  $node ['H'] = date('H', $dateTime);
	  $node ['i'] = date('i', $dateTime);
	  $node ['s'] = date('s', $dateTime);

	  if ($node ['date'] == date('d/m/Y')) {
	  if ($node ['H'] == date('H')) {
	  if ($node ['i'] == date('i')) {
	  $t = date('s') - $node ['s'];
	  return 'Cách đây ' . $t . ' giây';
	  } else {
	  $t = date('i') - $node ['i'];
	  return 'Cách đây ' . $t . ' phút';
	  }
	  } else {
	  $t = date('H') - $node ['H'];
	  return 'Cách đây ' . $t . ' tiếng';
	  }
	  } elseif (date('m/Y', $dateTime) == date('m/Y') && date('d', $dateTime) == date('d') - 1) {
	  return date('H:i', $dateTime) . ' hôm qua';
	  } else {
	  return date('H:i', $dateTime) . ' ngày ' . $node ['date'];
	  }
	  }
	 */

	public function time($dateTime1) {
		$dateTime = strtotime($dateTime1);
		$node ['date'] = date('d/m/Y', $dateTime);

		//Cach day
		$now = time();
		$t = $now - $dateTime;
		if ($t == 0) {
			return 'Cách đây vài giây';
		} else if ($t < 60) {
			return 'Cách đây ' . $t . ' giây';
		} else if ($t / 60 < 60) {
			return 'Cách đây ' . round($t / 60) . ' phút';
		}

		//Ngay, gio cu the
		if ($node ['date'] == date('d/m/Y')) {
			return date('H:i', $dateTime) . ' hôm nay';
		} elseif (date('d/m/Y') == date('d/m/Y', strtotime('+1 day', $dateTime))) {
			return date('H:i', $dateTime) . ' hôm qua';
		} else {
			if (date('Y', $dateTime) == date('Y')) {
				return date('H:i', $dateTime) . ' ngày ' . date('d/m', $dateTime);
			} else {
				return date('H:i', $dateTime) . ' ngày ' . $node ['date'];
			}
		}
	}

	public function timeForEvent($date) {
		if (date('m-d') == date('m-d', strtotime($date))) {
			return 'hôm nay';
		} else if (date('m-d', strtotime($date)) == date('m-d', strtotime('+1 day'))) {
			return 'ngày mai';
		} else {
			return date('d/m', strtotime($date));
		}
	}

	public function fullName($user, $useMe = FALSE) {
		$currentUser = $_SESSION['user'];
		if ($currentUser->username == $user->username && $useMe) {
			return 'tôi';
		}
		if ($user->fullname) {
			return $user->fullname;
		}
		if (!$user->fullnameType) {
			return $user->lastname . ' ' . $user->firstname;
		}
		return $user->firstname . ' ' . $user->lastname;
	}

	public function tFullName($user, $useMe = FALSE) {
		@$currentUser = $_SESSION['user'];
		if (@$currentUser->tUsername == $user->tUsername && $useMe) {
			return 'tôi';
		}
		if (!$user->tFullnameType) {
			return $user->tLastname . ' ' . $user->tFirstname;
		} else {
			return $user->tFirstname . ' ' . $user->tLastname;
		}
	}

	public function isMe() {
		if ($this->profile->id == $this->me->id) {
			return TRUE;
		}
		return FALSE;
	}

	public function sex($user) {
		if ($user->sex == 1) {
			return 'Nữ';
		} else {
			return 'Nam';
		}
	}

	public function date($date) {
		$t = strtotime($date);
		return date('j', $t) . ' tháng ' . date('n', $t) . ' năm ' . date('Y', $t);
	}

	public function avatar($user) {
		$r = new stdClass();
		if (!$user->avatar) {
			if ($user->sex == 2) {
				$r->big = STATIC_URL . 'images/no-avatar-male.gif';
				$r->small = STATIC_URL . 'images/no-avatar-male-small.gif';
			} else {
				$r->big = STATIC_URL . 'images/no-avatar-female.gif';
				$r->small = STATIC_URL . 'images/no-avatar-female-small.gif';
			}
		} else {
			global $config;
			$r->big = $config['upload'][$user->server]['path'] . $user->avatar;
			$r->small = $config['upload'][$user->server]['path'] . $user->avatarSmall;
		}

		return $r;
	}

	/**
	 * @param stdClass $node 
	 *              $node->image:       duong dan anh day du kich thuoc
	 *              $node->originFile:  duong dan anh goc
	 *              $node->thumbFile:   duong dan thumbnail
	 * @return stdClass: tuong ung
	 *            $f->normal
	 *            $f->origin
	 *            $f->thumb
	 */
	public function file($node) {
		$f = new stdClass();
		global $config;
		$f->normal = $config['upload'][$node->server]['path'] . $node->image;
		$f->origin = $config['upload'][$node->server]['path'] . $node->originFile;
		$f->thumb = $config['upload'][$node->server]['path'] . $node->thumbFile;

		return $f;
	}

	public function privacy($id) {
		if ($id == PRIVACY_ONLY_ME) {
			return 'Chỉ mình tôi';
		} elseif ($id == PRIVACY_EVERYONE) {
			return 'Tất cả mọi người';
		} elseif ($id == PRIVACY_FRIENDS_OF_FRIENDS) {
			return 'Bạn bè của bạn bè';
		} elseif ($id == PRIVACY_FRIENDS_ONLY) {
			return 'Chỉ bạn bè';
		} elseif ($id == PRIVACY_SPECIFIC_PEOPLE) {
			return 'Chỉ một số bạn bè được chỉ ra';
		}
	}

}
