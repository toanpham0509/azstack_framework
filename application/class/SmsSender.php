<?php

include_once 'Services/Twilio.php';

/**
 * @package		Borua
 * @copyright   Copyright (c) 2010 Anhxtanh3087
 * @author      Dau Ngoc Huy - Anhxtanh3087 - huydaungoc@gmail.com
 * @since       Feb 10, 2011
 * @version     $Id: NodeUtil.php 1 Feb 10, 2011 10:13:14 AM Anhxtanh3087 $
 */
class Class_SmsSender {

	public function __construct() {
		
	}

	/**
	 * 
	 * @param type $toPhoneNumber
	 * @param type $message
	 * @param type $provider 1=eSMS; 2=twilio; 3=vpmedia
	 */
	public function send($countryCode, $toPhoneNumber, $message, $provider) {
		/*
		if ($provider == 1) {
			$this->sendViaESMS('0' . $toPhoneNumber, $message);
			return;
		}
		if ($provider == 2) {
			$this->sendViaTwilio('+84' . $toPhoneNumber, $message);
			return;
		}
		if ($provider == 3) {
			$this->sendViaVpMedia('84' . $toPhoneNumber, $message);
			return;
		}
		*/
		
		error_log('send sms ....' . $countryCode .  $toPhoneNumber);
		$this->sendViaRabbitMQ($countryCode . $toPhoneNumber, $message, $countryCode);
	}
	
	public function sendViaRabbitMQ($toPhoneNumber, $message, $countryCode) {//to=84984636660
		$message = str_replace(' ', '+', $message);
		$res = file_get_contents("http://10.121.6.197:8916/?to=$toPhoneNumber&from=8088&message=$message&country_code=$countryCode");
		if ($res == '1') {
				$code = 100;
		} else {
				$code = 99;
		}
		$this->saveLogToDb($toPhoneNumber, $message, $code, null, '', 4);
		return $code;
	}

	public function sendViaTwilio($toPhoneNumber, $message) {
		$account_sid = 'ACb3af8dedc8a0a33c2e9944e4df0db99a'; //'AC1a4a2a03b117bfb84e2e82378ef7cf08'; ==>Huy
		$auth_token = '8b3d5df97af37960b64be4e96d8b8ca1'; //'f405a1dd99c4a555c2c7c05ba0cca4d3';
		$fromPhoneNumber = '+14844303080';
		$client = new Services_Twilio($account_sid, $auth_token);

		try {
			$res = $client->account->messages->create(array(
				'To' => $toPhoneNumber,
				'From' => $fromPhoneNumber,
				'Body' => $message,
			));

			if ($res->status == 'failed') {
				$code = 99;
			} else {
				$code = 100;
			}
		} catch (Exception $e) {
			//var_dump($e);
			error_log($e);
			//echo 'loi @@<br>';
			$code = 103;
		}

		$this->saveLogToDb($toPhoneNumber, $message, $code, null, $res->sid, 2);

		return $code;
	}
	
	/**
	 * 
	 * @param type $toPhoneNumber
	 * @param type $message
	 * @return int 100=thanh cong;  101=dang nhap khong thanh cong;  99=loi khong xac dinh
	 */
	public function sendViaVpMedia($toPhoneNumber, $message) {
		$from = '8788';
//		$to = '84903253475';
//		$msg = 'Thu nhan tin';
		$requestid = date('YmdHis') . rand(10000,99999);
		$username = 'bomchat';
		$password = 'l58d$fJ3QE!@kf';

		$client = new SoapClient('http://getway.vpmedia.vn/Getway.asmx?WSDL');
		$res = $client->SendMT(array('src'=>$from,'dest'=>$toPhoneNumber, 'msgbody'=>$message, 'requestid'=>$requestid, 'username'=>$username, 'password'=>$password));
		
		if(!$res){
			$code = 103;
		}else{
			$code = $res->SendMTResult;
			
			if($code == 1){
				$code = 100;
			}else if($code == -1){
				$code = 101;
			}else if($code == -101){
				$code = 99;
			}
		}
		
//		echo 'ket qua: ' . $res->SendMTResult;
		$this->saveLogToDb($toPhoneNumber, $message, $code, null, '0', 3);

		return $code;
	}

	/**
	 * 
	 * @param type $toPhoneNumber
	 * @param string $message
	 * @return int 100-request thanh cong; 99-loi khong xac dinh; 101-dang nhap khong thanh cong; 102-tai khoan bi khoa; 103-so du khong du; 104-Brandname khong dung; 1-loi parse XML response
	 */
	public function sendViaESMS($toPhoneNumber, $message) {
		//Visist http://http://esms.vn/SMSApi/ApiSendSMSNormal for more information about API
		$APIKey = "90D00164C01E5760CC5094DB055047"; //tai khoan: vietha999@gmail.com / duyhaucom
		$SecretKey = "9E712B6DCBD7EFC7572DF2D6C82DD1";
		$ch = curl_init();

		$SampleXml = "<RQST>"
				. "<APIKEY>" . $APIKey . "</APIKEY>"
				. "<SECRETKEY>" . $SecretKey . "</SECRETKEY>"
				. "<ISFLASH>0</ISFLASH>"
				. "<SMSTYPE>7</SMSTYPE>"
				. "<UNICODE>0</UNICODE>" //=0 là gửi không dấu, 1 là có dấu. Có dấu thì 70 ký tự 1 tin, không dấu thì 160 ký tự 1 tin
				. "<CONTENT>" . $message . "</CONTENT>"
				. "<CONTACTS>"
				. "<CUSTOMER>"
				. "<PHONE>" . $toPhoneNumber . "</PHONE>"
				. "</CUSTOMER>"
				. "</CONTACTS>"
				. "</RQST>";

		curl_setopt($ch, CURLOPT_URL, "http://api.esms.vn/MainService.svc/xml/SendMultipleMessage_V2/");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $SampleXml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/plain'));

		//*
		  $result = curl_exec($ch);
		  $xml = simplexml_load_string($result);

		  if ($xml === false) {
		  	//Error parsing XML
		  	$code = 1;
		  } else {
		  	$code = $xml->CodeResult;
		  }
		// * test thi tat di de do ton tien */
		$code = 100;

		$this->saveLogToDb($toPhoneNumber, $message, $code, $xml->ErrorMessage, $xml->SMSID, 1);

		return $code;
	}

	public function saveLogToDb($phoneNumber, $message, $result, $errorMessage, $proviverSmsId, $proviver) {
		$table = new Db_TableAbstract('sms_out');

		$row = array();
		$row['phoneNumber'] = $phoneNumber;
		$row['content'] = $message;
		$row['result'] = (int) $result;
		$row['errorMessage'] = $errorMessage;
		$row['proviverSmsId'] = $proviverSmsId;
		$row['proviver'] = $proviver;

		$row['created'] = date('Y-m-d H:i:s');

		$table->insert($row);
	}

}
