<?php

/**
 * Class Model_MPost
 */
class Model_MPost {

    /**
     * @var MysqliDb
     */
    private $db;

    /**
     * @var String
     */
    private $tableName;

    /**
     * Model_MPost constructor.
     */
    public function __construct() {
        global $db;
        $this->db = $db;

        $this->tableName = "post_tmp";
    }

    /**
     * @param $dataSearch
     * @return array
     */
    public function getPosts($dataSearch) {
        $this->db->orderBy("id", "DESC");
        $this->db->join("post_category", "az_post_category.categoryId = az_post_tmp.categoryId");
        if(isset($dataSearch['categoryId'])) {
            $this->db->where("az_post_tmp.categoryId = " .  $dataSearch['categoryId']);
        }
        return $this->db->get($this->tableName);
    }

    /**
     * @param $postId
     * @return array
     */
    public function getPost($postId) {
        $this->db->where("id", $postId);
        $this->db->join("post_category", "az_post_category.categoryId = az_post_tmp.categoryId");
        return $this->db->getOne($this->tableName);
    }

    /**
     * @param $url
     * @return array
     */
    public function getPostByUrl($url) {
        $this->db->where("url", $url);
        return $this->db->getOne($this->tableName);
    }

    /**
     * @param $data
     * @return bool
     */
    public function insert($data) {
        $this->db->insert($this->tableName, $data);
        return $this->db->getInsertId();
    }

    /**
     * @param $postId
     * @param $dataUpdate
     * @return bool
     */
    public function update($postId, $dataUpdate) {
        $this->db->where("id", $postId);
        return $this->db->update($this->tableName, $dataUpdate);
    }

    /**
     * @param $postId
     * @return bool
     */
    public function delete($postId) {
        $this->db->where("id", $postId);
        return $this->db->delete($this->tableName);
    }
}