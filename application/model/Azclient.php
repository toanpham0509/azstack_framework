<?php

class Model_Azclient {

    private $_db;

    function __construct() {
        global $config;
        $this->_db = new MysqliDb($config['db_sdk']);
    }

    public function countClients() {
        global $config;
        $count = $this->_db->rawQuery('SELECT count(*) as count from '. $config['db_sdk']['prefix'] .'az_client');
        return $count[0]['count'];
    }

}
