<?php

class Model_AzPricingOption {

	private $_db;

	function __construct() {
		global $db;
		$this->_db = $db;
	}
	
        public function update($data, $id) {
		$this->_db->where('id', $id);
		$this->_db->update('pricing_option', $data);
	}
        
        public function getById($id) {
		$this->_db->where('id', $id);
		return $this->_db->getOne('pricing_option');
	}
        
        public function getByLanguages($language) {
                $this->_db->where('language', $language);
                $this->_db->orderBy("id", "asc");
		return $this->_db->get('pricing_option');
	}
}
