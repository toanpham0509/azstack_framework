<?php

class Model_AzReassurance {

	private $_db;

	function __construct() {
		global $db;
		$this->_db = $db;
	}
	
        public function insert($data) {
		$id = $this->_db->insert('reassurance', $data);
		return $id;
	}

        public function delete($id) {
		$this->_db->where('id', $id);
		$this->_db->delete('reassurance');
	}
        
	public function update($data, $id) {
		$this->_db->where('id', $id);
		$this->_db->update('reassurance', $data);
	}
        
        public function getById($id) {
		$this->_db->where('id', $id);
		return $this->_db->getOne('reassurance');
	}
        
        public function getByType($type){
                $this->_db->where('type', $type);
		return $this->_db->get('reassurance');
	}
}
