<?php

class Model_AzTestimonials {

	private $_db;

	function __construct() {
		global $db;
		$this->_db = $db;
	}
	
        public function insert($data) {
		$id = $this->_db->insert('testimonials', $data);
		return $id;
	}

        public function delete($id, $language) {
		$this->_db->where('id', $id);
                $this->_db->where('language', $language);
		$this->_db->delete('testimonials');
	}
        
	public function update($data, $id, $language) {
		$this->_db->where('id', $id);
                $this->_db->where('language', $language);
		$this->_db->update('testimonials', $data);
	}
        
        public function getById($id, $language) {
		$this->_db->where('id', $id);
                $this->_db->where('language', $language);
		return $this->_db->getOne('testimonials');
	}
        
        public function getByLanguages($language) {
                $this->_db->where('language', $language);
                $this->_db->orderBy("id", "asc");
		return $this->_db->get('testimonials');
	}
}
