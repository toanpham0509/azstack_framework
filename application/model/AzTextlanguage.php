<?php

class Model_AzTextlanguage {

	private $_db;

	function __construct() {
		global $db;
		$this->_db = $db;
	}
	
        public function update($data, $type, $key, $language){
            $this->_db->where('`type`', $type);
            $this->_db->where('`key`', $key);
            $this->_db->where('`language`', $language);
            return $this->_db->update("text_language", $data);
        }
        
        public function getAllTextTypeKey($type, $key) {
                $this->_db->where('`type`', $type);
                $this->_db->where('`key`', $key);
		return $this->_db->get('text_language');
	}
        
        public function getAllTextLanguages($language) {
                $this->_db->where('language', $language);
		return $this->_db->get('text_language');
	}
}
