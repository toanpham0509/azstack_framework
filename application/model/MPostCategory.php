<?php

/**
 * Class Model_MPostCategory
 */
class Model_MPostCategory {

    /**
     * @var MysqliDb
     */
    private $db;

    private $tableName;

    /**
     * Model_MPostCategory constructor.
     */
    public function __construct() {
        global $db;
        $this->db =  $db;

        $this->tableName = "post_category";
    }

    /**
     * @return array
     */
    public function getCategories($language = null) {
        if($language != null) {
            $this->db->where("language", $language);
        }
        $this->db->orderBy("categoryId", "DESC");
        return $this->db->get($this->tableName);
    }

    /**
     * @param $categoryId
     * @return array
     */
    public function getCategory($categoryId) {
        $this->db->where("categoryId", $categoryId, "=");
        return $this->db->getOne($this->tableName);
    }

    /**
     * @param $dataUpdate
     * @return bool
     */
    public function addNew($dataUpdate) {
        $this->db->insert($this->tableName, $dataUpdate);
        return $this->db->getInsertId();
    }

    /**
     * @param $categoryId
     * @param $dataUpdate
     */
    public function update($categoryId, $dataUpdate) {
        $this->db->where("categoryId", $categoryId, "=");
        $this->db->update($this->tableName, $dataUpdate);
    }

    /**
     * @param $categoryId
     */
    public function delete($categoryId) {
        $this->db->where("categoryId", $categoryId, "=");
        $this->db->delete($this->tableName);
    }

    /**
     * @return array
     */
    public function getLanguages() {
        global $config;
        return $config['languageAll'];
//        $this->db->groupBy("language");
//        $this->db->pageLimit = 100;
//        return $this->db->get($this->tableName, null, "language");
    }
}