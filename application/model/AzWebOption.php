<?php

/**
 * Class Model_AzWebOption
 */
class Model_AzWebOption {

    private $options;

    /**
     * @var MysqliDb
     */
    private $db;

    /**
     * Model_AzWebOption constructor.
     */
    public function __construct() {
        global $db;
        $this->db = $db;

        $this->options = array("key", "value", "type", "language");
    }

    /**
     * @param $key
     * @param $type
     * @param $language
     */
    public function getOption($key, $type, $language) {
        $this->db->where("`key` = '$key'");
        $this->db->where("`type` = '$type'");
        $this->db->where("`language` = '$language'");

        return $this->db->getOne("text_language");
    }

    /**
     * @param $data
     * @return array
     */
    public function getOptions($data) {
        if(!empty($data)) {
            foreach ($this->options as $item) {
                if(isset($data[$item]) && strlen($data[$item])) {
                    $this->db->where("`$item` = \"" . $data[$item] . "\"");
                }
            }
        }
        return $this->db->get("text_language");
    }

    /**
     * @param $key
     * @param $type
     * @param $language
     */
    public function deleteOption($key, $type, $language) {
        $this->db->where("`key` = '$key'");
        $this->db->where("`type` = '$type'");
        $this->db->where("`language` = '$language'");

        return $this->db->delete("text_language");
    }

    /**
     * @param $key
     * @param $type
     * @param $language
     * @param $value
     */
    public function updateOption($key, $type, $language, $dataUpdate) {
        $this->db->where("`key` = '$key'");
        $this->db->where("`type` = '$type'");
        $this->db->where("`language` = '$language'");

        $this->db->update("text_language", $dataUpdate);
    }

    /**
     * @param $data
     * @return bool
     */
    public function insert($data) {
        return $this->db->insert("text_language", $data);
    }
}