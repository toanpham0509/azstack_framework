<?php

class Model_AzService {

	private $_db;

	function __construct() {
		global $db;
		$this->_db = $db;
	}
        
        public function update($data, $id, $language) {
		$this->_db->where('id', $id);
                $this->_db->where('language', $language);
		$this->_db->update('service', $data);
	}
        
        public function getById($id, $language) {
		$this->_db->where('id', $id);
                $this->_db->where('language', $language);
		return $this->_db->getOne('service');
	}
        
        public function getAllByLanguages($language) {
                $this->_db->where('language', $language);
                $this->_db->orderBy("id", "asc");
		return $this->_db->get('service');
	}
}
