<?php

class Model_AzQualityguarantee {

	private $_db;

	function __construct() {
		global $db;
		$this->_db = $db;
	}
	
        public function insert($data) {
		$id = $this->_db->insert('quality_guarantee', $data);
		return $id;
	}

        public function delete($id) {
		$this->_db->where('id', $id);
		$this->_db->delete('quality_guarantee');
	}
        
	public function update($data, $id) {
		$this->_db->where('id', $id);
		$this->_db->update('quality_guarantee', $data);
	}
        
        public function getById($id) {
		$this->_db->where('id', $id);
		return $this->_db->getOne('quality_guarantee');
	}
        
        public function getByTypeLanguage($type, $language){
                $this->_db->where('type', $type);
                $this->_db->where('language', $language);
		return $this->_db->get('quality_guarantee');
	}
}
