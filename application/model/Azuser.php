<?php

class Model_Azuser {

    private $_db;

    function __construct() {
        global $config;
        $this->_db = new MysqliDb($config['db_sdk']);
    }

    public function countUsers() {
        global $config;
        $count = $this->_db->rawQuery('SELECT count(*) as count from '. $config['db_sdk']['prefix'] .'user');
        return $count[0]['count'];
    }

}
