<?php

/**
 * Class Model_MSession
 */
class Model_MSession {

    /**
     * Model_MSession constructor.
     */
    public function __construct() {
    }

    /**
     * @param $dataArray
     */
    public function setSession($dataArray) {
        ob_start();
        if(is_array($dataArray) && !empty($dataArray)) {
            foreach($dataArray as $key => $value) {
                $_SESSION['azs'][$key] = $value;
            }
        }
    }

    /**
     *
     */
    public function unsetSession() {
        ob_start();
        unset($_SESSION['azs']);
    }
}