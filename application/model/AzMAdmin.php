<?php

/**
 * Class Model_AzMAdmin
 */
class Model_AzMAdmin {

    /**
     * @var Object
     */
    private $db;

    /**
     * Model_AzMAdmin constructor.
     */
    public function __construct() {
        global $db;
        $this->db =  $db;
    }

    /**
     * @param $email
     * @return mixed
     */
    public function getAdmin($email) {
        $this->db->where("email", $email, "=");
        return $this->db->getOne("user");
    }
}