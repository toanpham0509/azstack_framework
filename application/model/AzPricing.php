<?php

class Model_AzPricing {

	private $_db;

	function __construct() {
		global $db;
		$this->_db = $db;
	}
	
        public function update($data, $id, $language) {
		$this->_db->where('id', $id);
                $this->_db->where('language', $language);
		$this->_db->update('pricing', $data);
	}
        
        public function getById($id, $language) {
		$this->_db->where('id', $id);
                $this->_db->where('language', $language);
		return $this->_db->getOne('pricing');
	}
        
        public function getByLanguages($language) {
                $this->_db->where('language', $language);
                $this->_db->orderBy("id", "asc");
		return $this->_db->get('pricing');
	}
}
