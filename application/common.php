<?php

/**
 * @package		Borua
 * @copyright   Copyright (c) 2010 Anhxtanh3087
 * @author      Dau Ngoc Huy - Anhxtanh3087 - huydaungoc@gmail.com
 * @since       Feb 14, 2011
 * @version     $Id: common.php 1 Feb 14, 2011 2:43:36 PM Anhxtanh3087 $
 */
function dateTimeExplode($node) {
	$dateTime = strtotime($node ['created']);
	$node ['date'] = date("d/m/Y", $dateTime);
	$node ['time'] = date("H:i", $dateTime);
	return $node;
}

function setTemplatePath() {
	//Kiem tra dung mobile?
	$mobileDetect = new Class_MobileDetect();
	if (!isset($_SESSION['isMobileBrowser'])) {
		$_SESSION['isMobileBrowser'] = $mobileDetect->DetectMobileQuick();
		$_SESSION['mobileBrowser'] = $_SESSION['isMobileBrowser']; //Lưu có phải là mobile thật ko? Ko phụ thuộc người dùng chọn
	}
	
	/*
	if($mobileDetect->DetectAndroid()){
		header("Location: https://play.google.com/store/apps/details?id=com.bomchat");
		exit();
	}
	
	if($mobileDetect->DetectIos()){
		header("Location: https://itunes.apple.com/us/app/bomchat-goi-ien-hd-mien-phi/id894379951?ls=1&mt=8");
		exit();
	}
	*/
	
	
	$_SESSION['isMobileBrowser'] = false; //test
	
	if (!defined(TPL_PATH)) {
		global $config;
		if ($_SESSION['isMobileBrowser']) {
			define('TPL_NAME', $config['mobileTemplate']);
		} else {
			define('TPL_NAME', $config['template']);
		}
		define('TPL_PATH', ROOT_PATH . 'template/' . TPL_NAME . '/');
	}
	Controller::$layoutPath = array(TPL_PATH . '_layout/', ROOT_PATH . 'administrator/template/_layout/');
	View::$path = array(TPL_PATH, ROOT_PATH . 'administrator/template/');
}

function checkLogged() {
	//Check logged in session
	if ($_SESSION['user']) {
		return true;
	}

	//Check logged in cookie
//	if (!empty($_COOKIE ['expirationDate']) && $_COOKIE ['expirationDate'] > time()) {
//		//lay thong tin tu cookie
//		$username = $_COOKIE ['username'];
//		$expirationDate = $_COOKIE ['expirationDate'];
//		$hash = $_COOKIE ['hash'];
//
//		//lay thong tin user tu DB
//		$modelUser = new Model_User ();
//		$user = $modelUser->getByUsername(userFilter($username));
//		if ($hash == hashCookie($user->password, $username, $expirationDate)) {
//			$_SESSION['user'] = $user;
//			return true;
//		}
//	}
	return false;
}

function checkEvent($user) {
	$modelDaemon = new Model_Daemon();
	$modelDaemon->addToQueueIfNotExist($user, 1);
}

function userFilter($username) {
	return preg_replace('/[^a-z0-9\._\-]+/i', '', $username);
}

function hashCookie($password, $username, $expirationDate) {
	return md5($password . $username . $expirationDate . $_SERVER['REMOTE_ADDR'] . 'DauHuy');
}

/**
 * $data mảng dữ liệu gồm các object và chỉ số: ($obj1->index, $obj1->data)
 * $key tên trường chứa chỉ số để sắp xếp ('index')
 * $order ASC - tăng dần, DESC - giảm dần
 * */
function objectSort($data, $key, $order = 'ASC') {
	for ($i = count($data) - 1; $i >= 0; $i--) {
		$swapped = false;
		for ($j = 0; $j < $i; $j++) {
			if ($order == 'ASC') {
				if ($data[$j]->$key > $data[$j + 1]->$key) {
					$tmp = $data[$j];
					$data[$j] = $data[$j + 1];
					$data[$j + 1] = $tmp;
					$swapped = true;
				}
			} else {
				if ($data[$j]->$key < $data[$j + 1]->$key) {
					$tmp = $data[$j];
					$data[$j] = $data[$j + 1];
					$data[$j + 1] = $tmp;
					$swapped = true;
				}
			}
		}
		if (!$swapped)
			return $data;
	}

	return $data;
}


function isValidEmail($email){ 
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}


