<?php

class IndexController extends Controller {

    /**
     * @var Cache_Memcached
     */
    private $_cache;

    protected function _init() {
        $this->layout->name = 'azstack';
        global $config, $cache;
        $this->_cache = $cache;

        if (!isset($_SESSION['language'])) {
            $_SESSION['language'] = $config['language'];
        }

        $this->language = $_SESSION['language'];

        $lang = $this->_cache->load('lang[' . $this->language . ']');
        $link = $this->_cache->load('link[' . $this->language . ']');
        if (!$lang || !$link) {
            $loadText_language = $this->_loadText_language();
            $lang = $loadText_language['lang'];
            $link = $loadText_language['link'];
            $this->_cache->save($lang, 'lang[' . $this->language . ']', array(), 60 * 60 * 24);
            $this->_cache->save($link, 'link[' . $this->language . ']', array(), 60 * 60 * 24);
        }
        $this->layout->lang = $lang;
        $this->view->lang = $lang;
        $this->layout->link = $link;
        $this->view->link = $link;
    }

    function indexAction() {
        $this->layout->title = $this->view->lang['header']['website-title'];
        $modelReassurance = new Model_AzReassurance();
        $this->view->reassurance_types = array("trusted-by", "partners-support");
        $reassurance = $this->_cache->load('reassurance');
        if (!$reassurance) {
            foreach ($this->view->reassurance_types as $type) {
                $reassurance[$type] = $modelReassurance->getByType($type);
            }
            $this->_cache->save($reassurance, 'reassurance', array(), 60 * 60 * 24);
        }
        $this->view->reassurance = $reassurance;

        $modelQualityguarantee = new Model_AzQualityguarantee();
        $this->view->quality_guarantee_types = array("high-quality", "fast-affordable", "easily-accessible");
        $quality_guarantee = $this->_cache->load('quality_guarantee[' . $this->language . ']');
        if (!$quality_guarantee) {
            foreach ($this->view->quality_guarantee_types as $type) {
                $quality_guarantee[$type] = $modelQualityguarantee->getByTypeLanguage($type, $this->language);
            }
            $this->_cache->save($quality_guarantee, 'quality_guarantee[' . $this->language . ']', array(), 60 * 60 * 24);
        }
        $this->view->quality_guarantee = $quality_guarantee;

        $modelService = new Model_AzService();
        $service = $this->_cache->load('service[' . $this->language . ']');
        if (!$service) {
            $service = $modelService->getAllByLanguages($this->language);
            $this->_cache->save($service, 'service[' . $this->language . ']', array(), 60 * 60 * 24);
        }
        $this->view->service = $service;

        $modelPricing = new Model_AzPricing();
        $pricing = $this->_cache->load('pricing[' . $this->language . ']');
        if (!$pricing) {
            $pricing = $modelPricing->getByLanguages($this->language);
            $this->_cache->save($pricing, 'pricing[' . $this->language . ']', array(), 60 * 60 * 24);
        }
        $this->view->pricing = $pricing;


        $modelPricingOption = new Model_AzPricingOption();
        $pricingOption = $this->_cache->load('pricingOption[' . $this->language . ']');
        if (!$pricingOption) {
            $pricingOption = $modelPricingOption->getByLanguages($this->language);
            $this->_cache->save($pricingOption, 'pricingOption[' . $this->language . ']', array(), 60 * 60 * 24);
        }
        $this->view->pricingOption = $pricingOption;

        $modelTestimonials = new Model_AzTestimonials();
        $testimonials = $this->_cache->load('testimonials[' . $this->language . ']');
        if (!$testimonials) {
            $testimonials = $modelTestimonials->getByLanguages($this->language);
            $this->_cache->save($testimonials, 'testimonials[' . $this->language . ']', array(), 60 * 60 * 24);
        }
        $this->view->testimonials = $testimonials;

        $countclient = $this->_cache->load('countclient');
        if (!$countclient) {
            $modelClient = new Model_Azclient();
            $countclient = strval($modelClient->countClients());
            $this->_cache->save($countclient, 'countclient', array(), 60 * 60 * 24);
        }
        $this->view->countclient = $countclient;

        $countuser = $this->_cache->load('countuser');
        if (!$countuser) {
            $modelUser = new Model_Azuser();
            $countuser = strval($modelUser->countUsers());
            for ($i = strlen($countuser); $i < 8; $i++) {
                $countuser = "0" . $countuser;
            }
            $countuser = array(
                array(" ", $countuser[0], $countuser[1]),
                array($countuser[2], $countuser[3], $countuser[4]),
                array($countuser[5], $countuser[6], $countuser[7])
            );
            $this->_cache->save($countuser, 'countuser', array(), 60 * 60 * 24);
        }
        $this->view->countuser = $countuser;
    }

    function i18nAction() {
        $language = $this->_getParam('language');
        if ($language) {
            switch ($language) {
                case "en":
                    $_SESSION['language'] = "en";
                    break;
                case "vi":
                    $_SESSION['language'] = "vi";
                    break;
                default:
                    break;
            }
        }
        $this->_redirect(BASE_URL);
    }

    /**
     * @return array
     */
    private function _loadText_language() {
        $model = new Model_AzTextlanguage();
        $lang = array();
        $link = array();
        foreach ($model->getAllTextLanguages($this->language) as $row) {
            $lang[$row['type']][$row['key']] = $row['value'];
            $link[$row['type']][$row['key']] = $row['link'];
        }
        return array("lang" => $lang, "link" => $link);
    }

    /**
     *
     */
    public function postAction() {
        $url = $this->_getParam("url");
        $mPost = new Model_MPost();
        $this->view->post = $mPost->getPostByUrl($url);
        $this->layout->title = $this->view->post['title'];

        $this->view->relatestPosts = $mPost->getPosts(array("categoryId" => $this->view->post['categoryId']));
    }
}
